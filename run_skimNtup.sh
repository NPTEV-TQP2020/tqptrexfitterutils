samples=""

# samples+=" ttbar_PP8_mt172p5_LEPrb_newTQPMC_truth_onlySel"

samples+=" ttbar_PP8_mt165p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt170p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt170p5_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt171p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt171p5_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt172p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt173p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt173p5_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt174p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt174p5_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt175p0_LEPrb_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt180p0_LEPrb_newTQPMC_truth_onlySel"

samples+=" ttbar_PP8_mt172p5_LEPrb_RadHi_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_mt172p5_LEPrb_RadLow_newTQPMC_truth_onlySel"
samples+=" ttbar_aMCatNLOP8_999464_LEPrb_truth_onlySel"
samples+=" ttbar_PP8_mt172p5_LEPrb_MECoff_globrec_newTQPMC_truth_onlySel"
samples+=" ttbar_PP8_411260_FsrSc0p5_truth_onlySel"
samples+=" ttbar_PP8_411261_FsrSc2p0_truth_onlySel"
samples+=" ttbar_PP8_411258_LEPrb_FsrSc0p5_truth_onlySel"
samples+=" ttbar_PP8_411259_LEPrb_FsrSc2p0_truth_onlySel"
samples+=" ttbar_PP8_411262_Monash_truth_onlySel"
samples+=" ttbar_PP8_411263_LEPrb_Monash_truth_onlySel"
samples+=" ttbar_PP8_Monash3_LEPrb_411267_truth_onlySel"

# samples+=" "  # rb vars
# samples+=" "  # rb vars
# samples+=" "  # UE
# samples+=" "  # CR


for smp in ${samples}
do
#     macro 'SkimNtuples.C("'${smp}'")'
    export OPT=${smp}
    qsubl skimNtuple.job -V
done
