config = {
    "branches": [
        "lep_pt",
        "met_met",
        "mtw",
        "nJets",
        "nSMT",
        "eventNumber",
        "runNumber",
        "mcChannelNumber",
        "ejets",
        "mujets"
        "doubleTag",
        "bTagOther",
        "ge4j",
        "ge3j",
        "ge2j",
        "ge4j1b",
        "ge3j1b",
        "ge2j1b",
    ],  # branches common to both particle and reco level samples
    
    "reco_branches": [
        "calib_jet_pt",
    ],  # branches for only particle or reco - must be list
    
    "particle_branches": [
        "selected_jets_pt",
    ],
    
    "reco_cuts": "1",  # cuts to apply
    
    "particle_cuts": "1",
    
    "reco_weight": "1",  # weights to be combined into one - will be called weight
    
    "particle_weight": "1",
    
    "reco_new_vars": [  # new variables to be created - a list of lists with two entries; 1) name of new variable, 2) how it is constructed
         ["calib_jet_pt", "SMT_jet_calibration(jet_pt, SMTmu_jetIdx,1.0)"],    
     
        ["ejets", "(ejets_2015||ejets_2016)"],
        ["mujets", "(mujets_2015||mujets_2016)"],
        ["doubleTag","(isbtagged_77[SMTmu_jetIdx]>0.5)"],
        ["bTagOther","((isbtagged_77[SMTmu_jetIdx]<0.5)||(isbtagged_77[SMTmu_jetIdx]>0.5 && nBTags_77>=2))"],
        ["weight_ttbar", "PFRew(SMTmu_Bmother_pdgid,mcChannelNumber,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,mcChannelNumber,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,mcChannelNumber,0)"],
        ["ge4j", "(nJets>=4)"],
        ["ge3j", "(nJets>=3)"],
        ["ge2j", "(nJets>=2)"],
        ["ge4j1b", "(nJets>=4&&nBTags_77>=1)"],
        ["ge3j1b", "(nJets>=3&&nBTags_77>=1)"],
        ["ge2j1b", "(nJets>=2&&nBTags_77>=1)"],
        
        
    ],
        
    "particle_new_vars": [
    ],
        
    "functions": [  # any ROOT macros which are required for cuts/weights/new variables
        "macros/PFBRRew.C",
        "macros/PTRel.C",
        "macros/DeltaR.C",
        "macros/LewFuncs.C",
        "macros/Average.C",
        "macros/TopPt.C",
    ],
}
