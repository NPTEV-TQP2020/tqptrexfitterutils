# Example script to create all slimmed ntuple inputs

# -- where to run:
batch=local
# batch=qsub2
# batch=qsubl

# -- input directories
recoPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b_skim4j1b1smtPt8"
dataPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b"
truthPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b"

# -- outptu path
outPath="/nas01/atlas/pinamonti/MySkimmedNtuples/LewisReco_v18/"

# -- list of nominal reco samples (on these all the systematics listed below will be ran, and systematic weights will be saved):
samples=""
# samples+=" ttbar_PP8_LEPrb_AFII"
# samples+=" ttbar_PP8_dilepton"
# samples+=" ttbar_PP8"
# samples+=" Wjets"
# samples+=" Zjets"
# samples+=" Diboson"
# samples+=" singleTop_tchan"
# samples+=" singleTop_schan"
# samples+=" singleTop_Wt"

# -- list of alternative reco samples
altSamples=""
# altSamples+=" ttbar_Sherpa ttbar_PP8_AFII_dilepton ttbar_PH7_AFII_dilepton ttbar_PP8_RadHi_AFII ttbar_PP8_RadLo_AFII ttbar_aMCatNLOP8_AFII_dilepton ttbar_aMCatNLOP8_AFII_dilepton"
# altSamples+=" ttbar_PH713_AFII"

# -- data and fake samples
dataSamples=""
# dataSamples+=" data15 data16"
# dataSamples+=" fakes15 fakes16"

# -- truth samples
truthSampes=""
# truthSampes+=" ttbar_PP8_mt172p5_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt172p5_newTQPMC_truth_onlySel"
# truthSampes+=" ttbar_PP8_mt172p5_LEPrb_RadHi_newTQPMC_truth_onlySel ttbar_PP8_mt172p5_LEPrb_RadLow_newTQPMC_truth_onlySel"
# truthSampes+=" ttbar_PP8_mt172p5_LEPrb_MECoff_globrec_newTQPMC_truth_onlySel ttbar_aMCatNLOP8_999464_LEPrb_truth_onlySel"
# truthSampes+=" ttbar_PP8_999564_LEPrb_rb1p071_truth_onlySel ttbar_PP8_999565_LEPrb_rb1p029_truth_onlySel"
# truthSampes+=" ttbar_PP8_mt165p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt170p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt170p5_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt171p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt171p5_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt172p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt173p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt173p5_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt174p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt174p5_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt175p0_LEPrb_newTQPMC_truth_onlySel ttbar_PP8_mt180p0_LEPrb_newTQPMC_truth_onlySel"

# -- list of all systematic trees
systematics=""
# systematics+=" nominal"
# systematics+=" JET_JER_SINGLE_NP__1up"
# systematics+=" JET_29NP_ByCategory_JET_Pileup_RhoTopology__1up"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical1__1up JET_29NP_ByCategory_JET_EffectiveNP_Statistical2__1up JET_29NP_ByCategory_JET_EffectiveNP_Statistical3__1up JET_29NP_ByCategory_JET_EffectiveNP_Statistical4__1up JET_29NP_ByCategory_JET_EffectiveNP_Statistical5__1up JET_29NP_ByCategory_JET_EffectiveNP_Statistical6__1up JET_29NP_ByCategory_JET_EffectiveNP_Statistical7__1up"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical1__1down JET_29NP_ByCategory_JET_EffectiveNP_Statistical2__1down JET_29NP_ByCategory_JET_EffectiveNP_Statistical3__1down JET_29NP_ByCategory_JET_EffectiveNP_Statistical4__1down JET_29NP_ByCategory_JET_EffectiveNP_Statistical5__1down JET_29NP_ByCategory_JET_EffectiveNP_Statistical6__1down JET_29NP_ByCategory_JET_EffectiveNP_Statistical7__1down"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Detector1__1up JET_29NP_ByCategory_JET_EffectiveNP_Detector2__1up"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Detector1__1down JET_29NP_ByCategory_JET_EffectiveNP_Detector2__1down"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed1__1up JET_29NP_ByCategory_JET_EffectiveNP_Mixed2__1up JET_29NP_ByCategory_JET_EffectiveNP_Mixed3__1up"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed1__1down JET_29NP_ByCategory_JET_EffectiveNP_Mixed2__1down JET_29NP_ByCategory_JET_EffectiveNP_Mixed3__1down"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling1__1up JET_29NP_ByCategory_JET_EffectiveNP_Modelling2__1up JET_29NP_ByCategory_JET_EffectiveNP_Modelling3__1up JET_29NP_ByCategory_JET_EffectiveNP_Modelling4__1up"
# systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling1__1down JET_29NP_ByCategory_JET_EffectiveNP_Modelling2__1down JET_29NP_ByCategory_JET_EffectiveNP_Modelling3__1down JET_29NP_ByCategory_JET_EffectiveNP_Modelling4__1down"
# systematics+=" JET_29NP_ByCategory_JET_BJES_Response__1up JET_29NP_ByCategory_JET_Flavor_Composition__1up JET_29NP_ByCategory_JET_Flavor_Response__1up"
# systematics+=" JET_29NP_ByCategory_JET_BJES_Response__1down JET_29NP_ByCategory_JET_Flavor_Composition__1down JET_29NP_ByCategory_JET_Flavor_Response__1down"
# systematics+=" JET_29NP_ByCategory_JET_Pileup_OffsetMu__1up JET_29NP_ByCategory_JET_Pileup_OffsetNPV__1up JET_29NP_ByCategory_JET_Pileup_PtTerm__1up"
# systematics+=" JET_29NP_ByCategory_JET_Pileup_OffsetMu__1down JET_29NP_ByCategory_JET_Pileup_OffsetNPV__1down JET_29NP_ByCategory_JET_Pileup_PtTerm__1down JET_29NP_ByCategory_JET_Pileup_RhoTopology__1down"
# systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_Modelling__1up JET_29NP_ByCategory_JET_EtaIntercalibration_NonClosure__1up JET_29NP_ByCategory_JET_EtaIntercalibration_TotalStat__1up"
# systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_Modelling__1down JET_29NP_ByCategory_JET_EtaIntercalibration_NonClosure__1down JET_29NP_ByCategory_JET_EtaIntercalibration_TotalStat__1down"
# systematics+=" JET_29NP_ByCategory_JET_PunchThrough_MC15__1up JET_29NP_ByCategory_JET_PunchThrough_AFII__1up JET_29NP_ByCategory_JET_SingleParticle_HighPt__1up"
# systematics+=" JET_29NP_ByCategory_JET_PunchThrough_MC15__1down JET_29NP_ByCategory_JET_PunchThrough_AFII__1down JET_29NP_ByCategory_JET_SingleParticle_HighPt__1down"
# systematics+=" MET_SoftTrk_ResoPara MET_SoftTrk_ResoPerp MET_SoftTrk_ScaleUp MET_SoftTrk_ScaleDown"
# systematics+=" EG_SCALE_ALL__1up EG_RESOLUTION_ALL__1up"
# systematics+=" EG_SCALE_ALL__1down EG_RESOLUTION_ALL__1down"
# systematics+=" MUON_SCALE__1up MUON_MS__1up MUON_SAGITTA_RESBIAS__1up MUON_SAGITTA_RHO__1up"
# systematics+=" MUON_SCALE__1down MUON_ID__1down MUON_MS__1down MUON_SAGITTA_RESBIAS__1down MUON_SAGITTA_RHO__1down"

# -- FINISH CONFIG --

for smp in $samples
do
    for syst in ${systematics}
    do
        suf=""
        config="michele_config"
        if [[ $syst != "nominal" ]]
        then
            suf="__${syst}"
            config="michele_config_systematics"
        fi
        # backup any previous output with same name
        mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
        #
        export OPT=" -t ${syst} -f ${recoPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config}"
        echo ""
        echo "...................................................."
        echo "Running: python ntuple_slimmer.py ${OPT}"
        echo ""
        if [[ $batch == "local" ]]
        then
            eval python ntuple_slimmer.py ${OPT}
        else
            eval ${batch} ./ntuple_slimmer.job -V
        fi
        echo ""
    done
done 

# -------------------------------------------------------

# Now alternative reco samples
for smp in $altSamples
do
    suf=""
    config="michele_config_systematics"
    syst="nominal"
    # backup any previous output with same name
    mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
    #
    export OPT=" -t ${syst} -f ${recoPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config}"
    echo ""
    echo "...................................................."
    echo "Running: python ntuple_slimmer.py ${OPT}"
    echo ""
    if [[ $batch == "local" ]]
    then
        eval python ntuple_slimmer.py ${OPT}
    else
        eval ${batch} ./ntuple_slimmer.job -V
    fi
    echo ""
done

# -------------------------------------------------------

# Now data samples
for smp in $dataSamples
do
    suf=""
    config="michele_config_data"
    syst="nominal"
    if [[ $smp == *fakes* ]]
    then
        syst="nominal_Loose"
        config="michele_config_fakes"
    fi
    # backup any previous output with same name
    mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
    #
    export OPT=" -t ${syst} -f ${dataPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config}"
    echo ""
    echo "...................................................."
    echo "Running: python ntuple_slimmer.py ${OPT}"
    echo ""
    if [[ $batch == "local" ]]
    then
        eval python ntuple_slimmer.py ${OPT}
    else
        eval ${batch} ./ntuple_slimmer.job -V
    fi
    echo ""
done

# -------------------------------------------------------

# Now truth samples
for smp in $truthSampes
do
    suf=""
    config="michele_config"
    syst="particleLevelTree"
    # backup any previous output with same name
    mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
    #
    export OPT=" -t ${syst} -f ${truthPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config}"
    echo ""
    echo "...................................................."
    echo "Running: python ntuple_slimmer.py ${OPT}"
    echo ""
    if [[ $batch == "local" ]]
    then
        eval python ntuple_slimmer.py ${OPT}
    else
        eval ${batch} ./ntuple_slimmer.job -V
    fi
    echo ""
done
