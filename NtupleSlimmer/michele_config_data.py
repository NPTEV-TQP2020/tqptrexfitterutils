config = {
    "branches": [
        "Mlmu",
        "lep_pt",
        "lep_eta",
        "met_met",
        "mtw",
        "SMTmu_pt",
        "SMTmu_eta",
        "SMTmu_pTrel",
        "SMTmu_dR",
        "smtjet_pt",
        "smtjet_eta",
        "smtjet_jvt",
        "dRlepSMTmu",
        "nJets",
        "nSMT",
        "Rlb",
        "LepTopReco_pt",     
        "eventNumber",
        "runNumber",
        "mcChannelNumber",
        "weight",
        "SS",
        "OS",
        "ejets",
        "mujets",
        "lep_charge",
        "SMTmu_charge",
        "doubleTag",
        "bTagOther",
        "SMTmu_z",
        "calib_jet_pt",
    ],  # branches common to both particle and reco level samples
    
    "reco_branches": [		
    ],  # branches for only particle or reco - must be list
    
    "particle_branches": [
    ],
    
    #"reco_cuts": "nSMT>=1 && nBTags_77>=1 && met_met>30e3 && (met_met+mtw)>60e3 && SMTmu_pt>8e3 && (calib_jet_pt[3]>30e3 || (SMTmu_jetIdx==3 && calib_jet_pt[2]>30e3)) && DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)<2. && calib_jet_pt[SMTmu_jetIdx]>25e3",  # cuts to apply
    "reco_cuts": "nSMT>=1 && nBTags_77>=1 && met_met>30e3 && (met_met+mtw)>60e3 && SMTmu_pt>8e3 && (calib_jet_pt[3]>30e3 || (SMTmu_jetIdx==3 && calib_jet_pt[2]>30e3))",  # cuts to apply
       
    "particle_cuts": "1",
    
    "reco_weight": "1",  # weights to be combined into one - will be called weight
    
    "particle_weight": "1",
    
    "reco_new_vars": [  # new variables to be created - a list of lists with two entries; 1) name of new variable, 2) how it is constructed
        ["smtjet_jvt","jet_jvt[SMTtagged_jet_index]"],
        ["calib_jet_pt", "SMT_jet_calibration(jet_pt, SMTmu_jetIdx, 1.0)"],
        ["smtjet_pt","calib_jet_pt[SMTmu_jetIdx]"],
        ["smtjet_eta","jet_eta[SMTmu_jetIdx]"],
        ["SMTmu_z","SMTmuZ(calib_jet_pt[SMTmu_jetIdx],jet_eta[SMTmu_jetIdx],jet_phi[SMTmu_jetIdx],jet_e[SMTmu_jetIdx],SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["LepTopReco_pt","TopPt(lep_pt,lep_phi,met_met,met_phi,smtjet_pt,jet_phi[SMTmu_jetIdx],SMTmu_pt,SMTmu_phi,1)"], #last number to decide if you want to sum 0, 1 or 2 times the soft muon pt
        ["dRlepSMTmu","DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["Rlb","calib_jet_pt[SMTmu_jetIdx]/((SMTmu_jetIdx==0)*Average(calib_jet_pt[1],calib_jet_pt[2],calib_jet_pt[3]) + (SMTmu_jetIdx==1)*Average(calib_jet_pt[0],calib_jet_pt[2],calib_jet_pt[3]) + (SMTmu_jetIdx==2)*Average(calib_jet_pt[0],calib_jet_pt[1],calib_jet_pt[3]) + (SMTmu_jetIdx==3)*Average(calib_jet_pt[0],calib_jet_pt[2],calib_jet_pt[2]))"],
        
        ["OS", "1.*(SMTmu_charge*lep_charge<0)+0."],
        ["SS", "1.*(SMTmu_charge*lep_charge>0)+0."],
        ["ejets", "(ejets_2015||ejets_2016)"],
        ["mujets", "(mujets_2015||mujets_2016)"],
        ["doubleTag","(isbtagged_77[SMTmu_jetIdx]>0.5)"],
        ["bTagOther","((isbtagged_77[SMTmu_jetIdx]<0.5)||(isbtagged_77[SMTmu_jetIdx]>0.5 && nBTags_77>=2))"],
        
        
    ],
        
    "particle_new_vars": [
		["smtjet_jvt","1."],
    ],
        
    "functions": [  # any ROOT macros which are required for cuts/weights/new variables
        "macros/PFBRRew.C",
        "macros/PTRel.C",
        "macros/DeltaR.C",
        "macros/LewFuncs.C",
        "macros/Average.C",
        "macros/TopPt.C",
    ],
}
