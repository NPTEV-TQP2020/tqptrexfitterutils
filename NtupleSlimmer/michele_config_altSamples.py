config = {
    "branches": [
        "Mlmu",
        "lep_pt",
        "lep_eta",
        "met_met",
        "mtw",
        "SMTmu_pt",
        "SMTmu_eta",
        "SMTmu_pTrel",
        "SMTmu_dR",
        "smtjet_pt",
        "smtjet_eta",
        "smtjet_jvt", 
        "smtjet_isTrueHS",
        "smtjet_truthPartonLabel",
        "dRlepSMTmu",
        "nJets",
        "nSMT",
        "Rlb",
        "LepTopReco_pt",
        "eventNumber",
        "runNumber",
        "mcChannelNumber",
        "weight",
        "SS",
        "OS",
        "SMTmu_originFlag",
        "SMTmu_origin",
        "weight_ttbar",
        "ejets",
        "mujets",
        "isDilep",
        "isLjets",
        "isFake*",
        "lep_charge",
        "SMTmu_charge",
        "doubleTag",
        "bTagOther", 
        "SMTmu_z",
        "calib_jet_pt",
        "mc_generator_weights", 
        #"truth_t_afterrad_pt",
        #"truth_tbar_afterrad_pt",
        #"ttbar_pT",       
    ],  # branches common to both particle and reco level samples
    
    "reco_branches": [
        "VjetsFlag",       
#       "isbtagged_77",
#        "jet_NumTrkPt500",
#       "jet_truthflav",

    ],  # branches for only particle or reco - must be list
    
    "particle_branches": [
		"SMTmu_xB",
    ],
    
     #"reco_cuts": "nSMT>=1 && nBTags_77>=1 && met_met>30e3 && (met_met+mtw)>60e3 && SMTmu_pt>8e3 && (calib_jet_pt[3]>30e3 || (SMTmu_jetIdx==3 && calib_jet_pt[2]>30e3)) && DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)<2. && calib_jet_pt[SMTmu_jetIdx]>25e3",  # cuts to apply
    "reco_cuts": "nSMT>=1 && nBTags_77>=1 && met_met>30e3 && (met_met+mtw)>60e3 && SMTmu_pt>8e3 && (calib_jet_pt[3]>30e3 || (SMTmu_jetIdx==3 && calib_jet_pt[2]>30e3))",  # cuts to apply
    
    #"particle_cuts": "lep_pt/1e3>27 && fabs(lep_eta)<2.5 && (fabs(lep_pdgId)==11 || fabs(lep_pdgId)==13) && nJets>=4 && met_met>30e3 && (met_met+mtw)>60e3 && nSMT>=1 && nBTags_77>=1 && SMTmu_pt>8e3 &&  (calib_jet_pt[3]>30e3 || (abs(calib_jet_pt[3]-smtjet_pt)<0.1 && calib_jet_pt[2]>30e3)) && DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)<2.",
    "particle_cuts": "lep_pt/1e3>27 && fabs(lep_eta)<2.5 && (fabs(lep_pdgId)==11 || fabs(lep_pdgId)==13) && nJets>=4 && met_met>30e3 && (met_met+mtw)>60e3 && nSMT>=1 && nBTags_77>=1 && SMTmu_pt>8e3 &&  (calib_jet_pt[3]>30e3 || (abs(calib_jet_pt[3]-smtjet_pt)<0.1 && calib_jet_pt[2]>30e3))",
        
    "reco_weight": "weight_mc*weight_normalise*weight_smtmuSF*weight_pileup*weight_jvt*weight_leptonSF*ChMisId_SF*weight_bTagSF_MV2c10_77",  # weights to be combined into one - will be called weight
    #"reco_weight": "weight_mc*weight_smtmuSF*weight_pileup*weight_jvt*weight_leptonSF*ChMisId_SF*weight_bTagSF_MV2c10_77",  # weights to be combined into one - will be called weight
    
    "particle_weight": "weight_normalise*weight_mc",
    
    "reco_new_vars": [  # new variables to be created - a list of lists with two entries; 1) name of new variable, 2) how it is constructed
        ["smtjet_jvt","jet_jvt[SMTtagged_jet_index]"],
        ["smtjet_isTrueHS","jet_isTrueHS[SMTtagged_jet_index]"],
        ["smtjet_truthPartonLabel","jet_truthPartonLabel[SMTtagged_jet_index]"],
        ["calib_jet_pt", "SMT_jet_calibration(jet_pt, SMTmu_jetIdx, 1.0)"],    
        ["smtjet_pt","calib_jet_pt[SMTmu_jetIdx]"],
        ["smtjet_eta","jet_eta[SMTmu_jetIdx]"],
        ["SMTmu_z","SMTmuZ(calib_jet_pt[SMTmu_jetIdx],jet_eta[SMTmu_jetIdx],jet_phi[SMTmu_jetIdx],jet_e[SMTmu_jetIdx],SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["LepTopReco_pt","TopPt(lep_pt,lep_phi,met_met,met_phi,smtjet_pt,jet_phi[SMTmu_jetIdx],SMTmu_pt,SMTmu_phi,1)"], #last number to decide if you want to sum 0, 1 or 2 times the soft muon pt
        ["dRlepSMTmu","DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["Rlb","calib_jet_pt[SMTmu_jetIdx]/((SMTmu_jetIdx==0)*Average(calib_jet_pt[1],calib_jet_pt[2],calib_jet_pt[3]) + (SMTmu_jetIdx==1)*Average(calib_jet_pt[0],calib_jet_pt[2],calib_jet_pt[3]) + (SMTmu_jetIdx==2)*Average(calib_jet_pt[0],calib_jet_pt[1],calib_jet_pt[3]) + (SMTmu_jetIdx==3)*Average(calib_jet_pt[0],calib_jet_pt[2],calib_jet_pt[2]))"],
        
        ["OS", "1.*(SMTmu_charge*lep_charge<0)+0."],
        ["SS", "1.*(SMTmu_charge*lep_charge>0)+0."],
        [
            "isDilep",
            "fabs(SMTmu_originFlag)==2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isLjets",
            "fabs(SMTmu_originFlag)!=2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFake",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFakeSherpa",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4) && (smtmu_truthmuIndex[SMTmu_smtIdx]<0 || (truthmu_origin[smtmu_truthmuIndex[SMTmu_smtIdx]]!=43))",
        ],
        ["isFakeNonTT", "SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4"],
        ["ejets", "(ejets_2015||ejets_2016)"],
        ["mujets", "(mujets_2015||mujets_2016)"],
        ["doubleTag","(isbtagged_77[SMTmu_jetIdx]>0.5)"],
        ["bTagOther","((isbtagged_77[SMTmu_jetIdx]<0.5)||(isbtagged_77[SMTmu_jetIdx]>0.5 && nBTags_77>=2))"],
        
        ["weight_ttbar", "PFRew(SMTmu_Bmother_pdgid,mcChannelNumber,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,mcChannelNumber,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,mcChannelNumber,0)"],
        
    ],
        
    "particle_new_vars": [
        ["smtjet_jvt","1."],
        ["calib_jet_pt", "SMT_jet_calibration(selected_jets_pt, 0, 1.0)"],
        ["SMTmu_z","SMTmuZ(smtjet_pt,smtjet_eta,smtjet_phi,smtjet_E,SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["OS", "1.*(SMTmu_charge*lep_charge<0)+0."],
        ["SS", "1.*(SMTmu_charge*lep_charge>0)+0."],
        [
            "isDilep",
            "fabs(SMTmu_originFlag)==2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isLjets",
            "fabs(SMTmu_originFlag)!=2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFake",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFakeSherpa",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4) && (smtmu_truthmuIndex[SMTmu_smtIdx]<0 || (truthmu_origin[smtmu_truthmuIndex[SMTmu_smtIdx]]!=43))",
        ],
        ["isFakeNonTT", "SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4"],
        ["ejets", "(abs(lep_pdgId)==11)"],
        ["mujets", "(abs(lep_pdgId)==11)"],
        ["doubleTag","(smtjet_flav==5)"],
        ["bTagOther","((smtjet_flav!=5)||(smtjet_flav==5 && nBTags_77>=2))"],
        
        ["weight_ttbar", "PFRew(SMTmu_Bmother_pdgid,mcChannelNumber,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,mcChannelNumber,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,mcChannelNumber,0)"],
        
    ],
        
    "functions": [  # any ROOT macros which are required for cuts/weights/new variables
        "macros/PFBRRew.C",
        "macros/PTRel.C",
        "macros/DeltaR.C",
        "macros/LewFuncs.C",
        "macros/Average.C",
        "macros/TopPt.C",
    ],
}
