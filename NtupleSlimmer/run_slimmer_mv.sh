# Example script to create all slimmed ntuple inputs

# -- where to run:
#batch=local
#batch=qsub2
#batch=qsubl
batch=qsubssdc

applyCalib=true
#applyCalib=false

# -- input directories
recoPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b_skim4j1b1smtPt8/"
dataPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b/"
truthPath="/nas01/atlas/vanadiam/Top/TruthAnalyzer_2.4.42/runPrivate/OfflineNtuplesTruthOfficial/"
#truthPath="/nas01/atlas/vanadiam/Top/TruthAnalyzer_2.4.42/runPrivate/OfflineNtuplesTruth040319/"
# -- outptut path
outPath=""
if [[ $applyCalib == true ]]
	then
		outPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b_skim4j1b1smtPt8_slimmedCalib260919/"

	else
		outPath="/nas01/atlas/vanadiam/Top/Offline_2.4.42/run/outDec18/ljets_ge4jge1b_skim4j1b1smtPt8_slimmedUncalib260919/"	
fi

outPathTruth="/nas01/atlas/vanadiam/Top/TruthAnalyzer_2.4.42/runPrivate/OfflineNtuplesTruthOfficial_slimmed260919/"

logfile=${outPath}/slimmer.log
logfileTruth=${outPathTruth}/slimmerTruth.log

mv ${logfile} ${logfile}_backup
mv ${logfileTruth} ${logfileTruth}_backup
touch ${logfile}
touch ${logfileTruth}

# -- list of nominal reco samples (on these all the systematics listed below will be ran, and systematic weights will be saved):
samples=""
samples+=" ttbar_PP8_LEPrb_AFIIext"
samples+=" ttbar_PP8_dilepton"
samples+=" ttbar_PP8"
samples+=" Wjets"
samples+=" Zjets"
samples+=" Diboson"
samples+=" singleTop_tchan"
samples+=" singleTop_schan"
samples+=" singleTop_Wt"

## -- list of alternative reco samples
altSamples=""
#altSamples+=" ttbar_PP8_LEPrb_mt174p5_AFIIext"
#altSamples+=" ttbar_Sherpa"
#altSamples+=" ttbar_PP8_AFII_dilepton"
#altSamples+=" ttbar_PH7_AFII_dilepton"
#altSamples+=" ttbar_PP8_RadHi_AFII" 
#altSamples+=" ttbar_PP8_RadLo_AFII" 
#altSamples+=" ttbar_aMCatNLOP8_AFII_dilepton"
#altSamples+=" singleTop_Wt_PowhegPythia8_interfDS singleTop_Wt_RadSystUp_AFII singleTop_Wt_RadSystDown_AFII singleTop_tchan_RadSystUp_AFII singleTop_tchan_RadSystDown_AFII singleTop_schan_RadSystUp_AFII singleTop_schan_RadSystDown_AFII singleTop_tchan_PowhegHerwigpp_AFII singleTop_tchan_aMcAtNloHerwigpp_AFII singleTop_Wt_PowhegHerwigpp_AFII"
#altSamples+=" ttbar_PH7_AFII"
#altSamples+=" ttbar_aMCatNLOP8_AFII" 
#altSamples+=" ttbar_PP8_m170_AFII"
#altSamples+=" ttbar_PP8_m175_AFII" 
#altSamples+=" ttbar_PP8_AFII"
#altSamples+=" ttbar_PH713_AFIIext"
#altSamples+=" ttbar_PP8_LEPrb_ISRDo_411250_AFIIext"
#altSamples+=" ttbar_PP8_LEPrb_ISRUp_411249_AFIIext"
#altSamples+=" ttbar_PP8_LEPrb_ISRDo_411250_AFII"
#altSamples+=" ttbar_PP8_LEPrb_ISRUp_411249_AFII"
#altSamples+=" ttbar_PP8_LEPrb_MECoff_grec_411257_AFIIext"
#altSamples+=" ttbar_aMCP8_LEPrb_411258_AFIIext"
#altSamples+=" ttbar_PP8_LEPrb_FsrSc1p414_411280_AFIIext"
#altSamples+=" ttbar_PP8_LEPrb_FsrSc0p707_411279_AFIIext"

# -- data and fake samples
dataSamples=""
#dataSamples+=" data15 data16"
#dataSamples+=" fakes15 fakes16"

# -- truth samples
truthSamples=""
#truthSamples+=" ttbar_aMCP8Old_truth_onlySel"
#truthSamples+=" ttbar_PH7_704_truth_onlySel"
#truthSamples+=" ttbar_PP8_RadHi_truth_onlySel"
#truthSamples+=" ttbar_PP8_RadLow_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt170p0_truth_onlySel"
#truthSamples+=" ttbar_PP8_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt175p0_truth_onlySel"

#truthSamples+=" ttbar_PH713_411168_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt172p5_LEPrb_411169_TRUTH1_truth_onlySel"

#truthSamples+=" ttbar_PP8_LEPrb_ISRUp_411249_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_ISRDo_411250_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_rb1p071_411251_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_rb1p029_411252_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_Var1Up_411253_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_Var1Do_411254_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_CRmax_411255_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_CRoff_411256_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_MECoff_411257_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_aMCatNLOP8_noShWe_LEPrb_411258_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_rb1p10_411259_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_rb1p00_411260_TRUTH1_truth_onlySel"

#truthSamples+=" ttbar_PP8_LEPrb_ShWeights_240_411240_TRUTH1_p3201_truth_onlySel_a"
#truthSamples+=" ttbar_PP8_LEPrb_ShWeights_240_411240_TRUTH1_p3201_truth_onlySel_b"
#truthSamples+=" ttbar_PP8_LEPrb_recToColOff_411248_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_FsrSc0p5_411261_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_FsrSc2p0_411262_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_FsrSc0p707_411263_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_FsrSc1p414_411264_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_Monash_411265_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_Monash_Peterson_411267_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_ERD_411268_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PH713_dipole_411269_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP6_411278_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_FsrSc1p414_411280_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_LEPrb_FsrSc0p707_411279_TRUTH1_p3201_truth_onlySel"
#truthSamples+=" ttbar_PP8_Monash_A14ISR_LEPrb_411281_TRUTH1_p3201_truth_onlySel"

#truthSamples+=" ttbar_PP8_999279_LEPrb_FsrSc0p707_truth_onlySel"

#truthSamples+=" ttbar_PP8_mt165p0_LEPrb_411246_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt169p0_LEPrb_411235_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt170p5_LEPrb_411236_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt171p0_LEPrb_411237_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt171p5_LEPrb_411238_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt172p0_LEPrb_411239_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt173p0_LEPrb_411241_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt173p5_LEPrb_411242_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt174p0_LEPrb_411243_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt174p5_LEPrb_411244_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt176p0_LEPrb_411245_TRUTH1_truth_onlySel"
#truthSamples+=" ttbar_PP8_mt180p0_LEPrb_411247_TRUTH1_truth_onlySel"


## -- list of all systematic trees
systematics=""
 systematics+=" nominal"
 #systematics+=" JET_29NP_ByCategory_JET_RelativeNonClosure_AFII__1up" #only AFII samples
 #systematics+=" JET_29NP_ByCategory_JET_RelativeNonClosure_AFII__1down" #only AFII samples
 #systematics+=" JET_29NP_ByCategory_JET_PunchThrough_AFII__1up"  #only AFII samples
 #systematics+=" JET_29NP_ByCategory_JET_PunchThrough_AFII__1down" #only FullSim samples
 #systematics+=" JET_29NP_ByCategory_JET_PunchThrough_MC15__1up"  #only AFII samples
 #systematics+=" JET_29NP_ByCategory_JET_PunchThrough_MC15__1down" #only FullSim samples
 #systematics+=" JET_JER_SINGLE_NP__1up"
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_RhoTopology__1up"
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_RhoTopology__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical1__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical2__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical3__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical4__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical5__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical6__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical7__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical1__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical2__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical3__1down" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical4__1down" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical5__1down" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical6__1down" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Statistical7__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Detector1__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Detector2__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Detector1__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Detector2__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed1__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed2__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed3__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed1__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed2__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Mixed3__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling1__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling2__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling3__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling4__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling1__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling2__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling3__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EffectiveNP_Modelling4__1down"
 #systematics+=" JET_29NP_ByCategory_JET_BJES_Response__1up"
 #systematics+=" JET_29NP_ByCategory_JET_BJES_Response__1down"
 #systematics+=" JET_29NP_ByCategory_JET_Flavor_Composition__1up"
 #systematics+=" JET_29NP_ByCategory_JET_Flavor_Response__1up"
 #systematics+=" JET_29NP_ByCategory_JET_Flavor_Composition__1down"
 #systematics+=" JET_29NP_ByCategory_JET_Flavor_Response__1down"
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_OffsetMu__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_OffsetNPV__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_PtTerm__1up"
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_OffsetMu__1down"
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_OffsetNPV__1down" 
 #systematics+=" JET_29NP_ByCategory_JET_Pileup_PtTerm__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_Modelling__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_NonClosure__1up" 
 #systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_TotalStat__1up"
 #systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_Modelling__1down" 
 #systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_NonClosure__1down"
 #systematics+=" JET_29NP_ByCategory_JET_EtaIntercalibration_TotalStat__1down"
 #systematics+=" JET_29NP_ByCategory_JET_SingleParticle_HighPt__1up"
 #systematics+=" JET_29NP_ByCategory_JET_SingleParticle_HighPt__1down"
 #systematics+=" MET_SoftTrk_ResoPara" 
 #systematics+=" MET_SoftTrk_ResoPerp" 
 #systematics+=" MET_SoftTrk_ScaleUp" 
 #systematics+=" MET_SoftTrk_ScaleDown"
 #systematics+=" EG_SCALE_ALL__1up"
 #systematics+=" EG_RESOLUTION_ALL__1up"
 #systematics+=" EG_SCALE_ALL__1down" 
 #systematics+=" EG_RESOLUTION_ALL__1down"
 #systematics+=" MUON_SCALE__1up" 
 #systematics+=" MUON_MS__1up" 
 #systematics+=" MUON_ID__1up" 
 #systematics+=" MUON_SAGITTA_RESBIAS__1up"
 #systematics+=" MUON_SAGITTA_RHO__1up"
 #systematics+=" MUON_SCALE__1down" 
 #systematics+=" MUON_ID__1down"
 #systematics+=" MUON_MS__1down" 
 #systematics+=" MUON_SAGITTA_RESBIAS__1down"
 #systematics+=" MUON_SAGITTA_RHO__1down"

#reweighting systematics needing special SMTjet calibration
rwtSystematics=""
#rwtSystematics+=" SMTJES_STAT1__1up"
#rwtSystematics+=" SMTJES_STAT1__1down"
#rwtSystematics+=" b_tag_0_up"
#rwtSystematics+=" b_tag_0_down"
rwtSystematics+=" pileup_up"
rwtSystematics+=" pileup_down"
#rwtSystematics+=" btocWS_up"
#rwtSystematics+=" btocWS_down"

if [[ $applyCalib == false ]]
then
	rwtSystematics="" #useless to do this if we don't want to apply the calibration
fi
# -- FINISH CONFIG --

for smp in $samples
do
    for syst in ${systematics}
    do
        suf=""
        config="michele_config"
        add=""
        if [[ $applyCalib ==  true ]]
        then
			#remember that we don't want to recalibrate samples used for dilepton or for ttbar fakes
			if [[ $smp == "ttbar_PP8"* ]] && [[ $smp != "ttbar_PP8" ]] && [[ $smp != "ttbar_PP8_dilepton" ]]
			then
				#standard calibration for everything
				add="--smtjetcalibcorr 0.967"
				#separate trees systematics needing an ad-hoc calibration		
				if [[ $syst == "JET_29NP_ByCategory_JET_BJES_Response__1up" ]]
				then
					add="--smtjetcalibcorr 0.963"
				fi
				if [[ $syst == "JET_29NP_ByCategory_JET_BJES_Response__1down" ]]
				then
					add="--smtjetcalibcorr 0.971"
				fi
				if [[ $syst == "JET_29NP_ByCategory_JET_Pileup_RhoTopology__1up" ]]
				then
					add="--smtjetcalibcorr 0.966"
				fi
				if [[ $syst == "JET_29NP_ByCategory_JET_Pileup_RhoTopology__1down" ]]
				then
					add="--smtjetcalibcorr 0.968"
				fi
				echo "changing calibration to "$add
				if [[ $syst == "JET_JER_SINGLE_NP__1up" ]]
				then
					add="--smtjetcalibcorr 0.965"
				fi
			fi
		fi
		
		if [[ $syst != "nominal" ]]
		then
			suf="__${syst}"
			config="michele_config_systematics"
		fi
        # backup any previous output with same name
        mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
        #
        if [[ $smp == "ttbar_PP8"* ]]
        then
			add=$add" --moreTTVariables"
		fi
        export OPT=" -t ${syst} -f ${recoPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config} $add"
        
        
        
        echo ""
        echo "...................................................."
        echo "Running: python ntuple_slimmer.py ${OPT}"
        echo ""
        if [[ $batch == "local" ]]
        then
            eval python ntuple_slimmer.py ${OPT}
        else
            eval ${batch} ./ntuple_slimmer.job -V
        fi
        echo ${OPT} >> $logfile
        echo ""
    done
done 
# -------------------------------------------------------
#reweighting systematics needing special SMTjet calibration
for smp in $samples
do
    for syst in ${rwtSystematics}
    do
        suf=""
        config="michele_config"
        add=""
        if [[ $applyCalib ==  true ]]
        then
			#remember that we don't want to recalibrate samples used for dilepton or for ttbar fakes
			if [[ $smp == "ttbar_PP8"* ]]  && [[ $smp != "ttbar_PP8" ]] && [[ $smp != "ttbar_PP8_dilepton" ]]
			then
				#standard calibration for everything
				add="--smtjetcalibcorr 0.967"
				#separate trees systematics needing an ad-hoc calibration
				if [[ $syst == "SMTJES_STAT1__1up" ]]
				then
					add="--smtjetcalibcorr 0.972"
				fi
				if [[ $syst == "SMTJES_STAT1__1down" ]]
				then
					add="--smtjetcalibcorr 0.962"
				fi
				if [[ $syst == "b_tag_0_up" ]]
				then
					add="--smtjetcalibcorr 0.962"
				fi
				if [[ $syst == "b_tag_0_down" ]]
				then
					add="--smtjetcalibcorr 0.972"
				fi
				if [[ $syst == "pileup_up" ]]
				then
					add="--smtjetcalibcorr 0.969"
				fi
				if [[ $syst == "pileup_down" ]]
				then
					add="--smtjetcalibcorr 0.965"
				fi
				if [[ $syst == "btocWS_up" ]]
				then
					add="--smtjetcalibcorr 0.964"
				fi
				if [[ $syst == "btocWS_down" ]]
				then
					add="--smtjetcalibcorr 0.970"
				fi
				echo "changing calibration to "$add
			fi
		fi
		tree="nominal"
		suf="__${syst}"
        # backup any previous output with same name
        mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
        #
        if [[ $smp == "ttbar"* ]] && [[ $smp != "ttbar_Sherpa" ]]
        then
			add=$add" --moreTTVariables"
		fi
        export OPT=" -t ${tree} -f ${recoPath}/${smp}.root -o ${outPath}/${smp}${suf}.root -c ${config} $add"
        echo ""
        echo "...................................................."
        echo "Running: python ntuple_slimmer.py ${OPT}"
        echo ""
        if [[ $batch == "local" ]]
        then
            eval python ntuple_slimmer.py ${OPT}
        else
            eval ${batch} ./ntuple_slimmer.job -V
        fi
        echo ${OPT} >> $logfile
        echo ""
    done
done 

# -------------------------------------------------------
# Now alternative reco samples
for smp in $altSamples
do
    suf=""
    config="michele_config_altSamples"
    if [[ $smp == "ttbar_aMCP8_LEPrb_411258_AFIIext" ]]
    then
     config="michele_config_altSamples_noWeightNorm"
    fi
    syst="nominal"
    add=""
    if [[ $applyCalib ==  true ]]
        then
			#remember that we don't want to recalibrate samples used for dilepton or for ttbar fakes
			if [[ $smp == "ttbar_"* ]] && [[ $smp != "ttbar_Sherpa" ]] &&  [[ $smp != *"dilepton"* ]] &&  [[ $smp != "ttbar_PP8_RadHi_AFII" ]] &&  [[ $smp != "ttbar_PP8_RadLo_AFII" ]] &&  [[ $smp != "ttbar_PH7_AFII" ]] &&  [[ $smp != "ttbar_aMCatNLOP8_AFII" ]] &&  [[ $smp != "ttbar_PP8_m170_AFII" ]] &&  [[ $smp != "ttbar_PP8_m175_AFII" ]] &&  [[ $smp != "ttbar_PP8_AFII" ]]
			then
				#standard calibration for everything
				add="--smtjetcalibcorr 0.967"
				#samples needing an ad-hoc calibration
				if [[ $smp == "ttbar_PP8_LEPrb_ISRUp_411249_AFII" ]]
				then
					add="--smtjetcalibcorr 0.968"
				fi
				if [[ $smp == "ttbar_PP8_LEPrb_ISRDo_411250_AFII" ]]
				then
					add="--smtjetcalibcorr 0.966"
				fi
				if [[ $smp == "ttbar_aMCP8_LEPrb_411258_AFII" ]] || [[ $smp == "ttbar_aMCP8_LEPrb_411258_AFIIext" ]]
				then
					#add="--smtjetcalibcorr 0.946"
					add="--smtjetcalibcorr 0.960"
				fi
				echo "changing calibration to "$add
				if [[ $smp == "ttbar_PH713_AFII" ]] || [[ $smp == "ttbar_PH713_AFIIext" ]]
				then
					add="--smtjetcalibcorr 0.969"
				fi
				
				
			fi
		fi
    # backup any previous output with same name
    mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
    #
    if [[ $smp == "ttbar"* ]] && [[ $smp != "ttbar_Sherpa" ]]
        then
			add=$add" --moreTTVariables"
	fi
    export OPT=" -t ${syst} -f ${recoPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config} $add"
    echo ""
    echo "...................................................."
    echo "Running: python ntuple_slimmer.py ${OPT}"
    echo ""
    
    if [[ $batch == "local" ]]
    then
        eval python ntuple_slimmer.py ${OPT}
    else
        eval ${batch} ./ntuple_slimmer.job -V
    fi
    echo ${OPT} >> $logfile
    echo ""
done

# -------------------------------------------------------

# Now data samples
for smp in $dataSamples
do
    suf=""
    config="michele_config_data"
    syst="nominal"
    if [[ $smp == *fakes* ]]
    then
        syst="nominal_Loose"
        config="michele_config_fakes"
    fi
    # backup any previous output with same name
    mv ${outPath}/${smp}${suf}.root ${outPath}/${smp}${suf}_backup.root
    #
    export OPT=" -t ${syst} -f ${dataPath}/${smp}${suf}.root -o ${outPath}/${smp}${suf}.root -c ${config}"
    echo ""
    echo "...................................................."
    echo "Running: python ntuple_slimmer.py ${OPT}"
    echo ""
    if [[ $batch == "local" ]]
    then
        eval python ntuple_slimmer.py ${OPT}
    else
        eval ${batch} ./ntuple_slimmer.job -V
    fi
    echo ${OPT} >> $logfile
    echo ""
done

# -------------------------------------------------------

# Now truth samples
for smp in $truthSamples
do
    suf=""
    config="michele_config"
    syst="particleLevelTree"
    # backup any previous output with same name
    mv ${outPathTruth}/${smp}${suf}.root ${outPathTruth}/${smp}${suf}_backup.root
    #
    add=""
    if [[ $smp == "ttbar"* ]] && [[ $smp != "ttbar_Sherpa" ]]
        then
			add=$add" --moreTTVariables"
	fi
    export OPT=" -t ${syst} -f ${truthPath}/${smp}${suf}.root -o ${outPathTruth}/${smp}${suf}.root -c ${config} $add"
    echo ""
    echo "...................................................."
    echo "Running: python ntuple_slimmer.py ${OPT}"
    echo ""
    if [[ $batch == "local" ]]
    then
        eval python ntuple_slimmer.py ${OPT}
    else
        eval ${batch} ./ntuple_slimmer.job -V
    fi
    echo ${OPT} >> $logfileTruth
    echo ""
done
