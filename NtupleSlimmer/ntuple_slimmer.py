import ROOT as r
import sys
from array import array
from optparse import OptionParser
import importlib
import os
import glob
import re
import time

def load_functions(function_list):
    try:
        for func in function_list:
            r.gROOT.ProcessLineSync(".L " + func + "+")
    except:
        print("No functions defined.")


def load_config(config_file_name):
    try:
        config_module = importlib.import_module(config_file_name)
        config = config_module.config
    except:
        print("Invalid config name! Please enter a valid config file name")
        sys.exit()

    return config


def rootify_branch_list(branch_list):
    r_branch_list = r.vector("string")()
    [r_branch_list.push_back(b) for b in branch_list]
    return r_branch_list


def get_file_size(path):
    size = 0
    for file in glob.glob(path):
        size += os.path.getsize(file)
    return size


def print_list(list_to_print):
    for l in list_to_print:
        print(l)


def build_list_from_regex(tree, regex):
    branch_list = []
    branches = tree.GetColumnNames()
    for branch in branches:
        if re.search(regex, branch):
            branch_list.append(branch)
    return branch_list


def prepare_branch_list(tree, branch_list):
    if branch_list[0] == "*":
        return [None]
    final_branch_list = list(branch_list)
    for branch in branch_list:
        if "*" in branch:
            branch_regex = branch.replace("*", ".*")
            branch_regex = "^" + branch_regex
            branch_regex = branch_regex + "$"
            regex_branches = build_list_from_regex(tree, branch_regex)
            final_branch_list.remove(branch)
            final_branch_list += regex_branches

    r_final_branch_list = rootify_branch_list(final_branch_list)
    return r_final_branch_list

def define_variables(df, new_vars):
    if not new_vars[0] is None:
        for i, var in enumerate(new_vars):
            df = df.Define(var[0], var[1])
        
    return df

def event_loop_slim(options, branch_list):
    chain = r.TChain(options.tree_name)
    input_files = glob.glob(options.file_name)
    for file in input_files:
        chain.Add(file)
    
    out_file = r.TFile(options.output_file_name)
    out_tree = chain.CloneTree(0)

    for i,entry in enumerate(chain):
        if i % 10000 == 0:
            print(i)
        if entry.SMTmu_pt > 8e3 and entry.nSMT >= 1:
            out_tree.Fill()
    
    out_tree.SetBranchStatus("*", 0)
    for branch in branch_list:
        out_tree.SetBranchStatus(branch, 1)
    
    out_file.Write()
    

r.gROOT.SetBatch(True)
# 
#r.ROOT.EnableImplicitMT()
r.ROOT.DisableImplicitMT()

if __name__ == "__main__":
    
    parser = OptionParser()

    parser.add_option("-f", "--file", dest="file_name", help="file name")
    parser.add_option("-t", "--tree", dest="tree_name", help="tree name")
    parser.add_option("-o", "--output", dest="output_file_name", help="Output file name")
    parser.add_option(
        "-c",
        "--config",
        dest="config_file_name",
        help="Config file name without extension",
        default=None,
    )
    parser.add_option("-e", "--events", dest="n_events", help="n_events", default=0)
    parser.add_option("-j", "--smtjetcalibcorr", dest="smtJetCalibCorr", help="smtJetCalibCorr", default=1)
    parser.add_option("-m","--moreTTVariables",action="store_true",dest="moreTTVariables")
    (options, args) = parser.parse_args()

    
    config = load_config(options.config_file_name)
    load_functions(config["functions"])


    if options.tree_name == "particleLevelTree":
        weight = config["particle_weight"]
        cut = config["particle_cuts"]
        branch_list = config["branches"] + config["particle_branches"]
        new_vars = config["particle_new_vars"]

    else:  # now assuming any tree that is NOT particleLevelTree is a reco tree
        weight = config["reco_weight"]
        cut = config["reco_cuts"]
        if not (len(config["reco_branches"]) <1 or config["reco_branches"][0] is None):
            branch_list = config["branches"] + config["reco_branches"]
        else:
            branch_list = config["branches"]
        new_vars = config["reco_new_vars"]
        
        if(options.smtJetCalibCorr!=1):
			for i in range(0,len(new_vars)):
				if(new_vars[i][0] is not "calib_jet_pt"):
					continue
				print "changing calibration"
				new_vars[i]=["calib_jet_pt", "SMT_jet_calibration(jet_pt, SMTmu_jetIdx,"+str(options.smtJetCalibCorr)+")"]
			print new_vars
	
    if(options.moreTTVariables):
		print "adding more ttbar variables"
		branch_list.append("truth_t_afterrad_pt")
		branch_list.append("truth_tbar_afterrad_pt")
		branch_list.append("ttbar_pT")
		branch_list.append("SMTmu_xB")
		branch_list.append("isST_truth")
		branch_list.append("isDT_truth")

    print("Slimming {0}".format(options.file_name))
    print("Applying cut {0}".format(cut))
    print("Branches to keep:")
    print_list(branch_list)

    start = time.time()

    df = r.RDataFrame(options.tree_name, options.file_name)
    if options.n_events > 0:
        r.ROOT.DisableImplicitMT() #Cannot run over range in MT
        df = df.Range(int(options.n_events))
   
    
    df = df.Define("weight", weight)
    df = define_variables(df, new_vars)
    df = df.Filter(cut)

    r_branch_list = prepare_branch_list(df, branch_list)
    if r_branch_list[0] is None:
        df.Snapshot(options.tree_name, options.output_file_name)
    else:
        df.Snapshot(options.tree_name, options.output_file_name, r_branch_list)


        
    original_size = get_file_size(options.file_name)
    new_size = get_file_size(options.output_file_name)
    end = time.time()
    print(
        "Original size was {0:.1f}mb, new file is {1:.1f}mb. Reduction of {2:.3f}%.".format(
            original_size / 1e6,
            new_size / 1e6,
            100 * (1 - (float(new_size) / original_size)),
        )
    )
    print("time taken {0}s".format(end-start))
