config = {
    "branches": [
        "lep_pt",
        "met_met",
        "mtw",
        "nJets",
        "nSMT",
        "eventNumber",
        "runNumber",
        "mcChannelNumber",
        "weight",
        "weight_ttbar",
        "ejets",
        "mujets",
        "isDilep",
        "isLjets",
        "isFake*",
        "doubleTag",
        "bTagOther",
        "ge4j",
        "ge3j",
        "ge2j",
        "ge4j1b",
        "ge3j1b",
        "ge2j1b",


       
    ],  # branches common to both particle and reco level samples
    
    "reco_branches": [
        "calib_jet_pt",
        "VjetsFlag",

    ],  # branches for only particle or reco - must be list
    
    "particle_branches": [
        "selected_jets_pt",
        "mc_generator_weights",
        "SMTmu_xB",
    ],
    
    "reco_cuts": "1",  # cuts to apply
    
    "particle_cuts": "lep_pt/1e3>27 && fabs(lep_eta)<2.5 && (fabs(lep_pdgId)==11 || fabs(lep_pdgId)==13) && nJets>=4 && met_met>30e3 && (met_met+mtw)>60e3 && nSMT>=1 && nBTags_77>=1 && SMTmu_pt>8e3 &&  (selected_jets_pt[3]>30e3 || (abs(selected_jets_pt[3]-smtjet_pt)<0.1 && selected_jets_pt[2]>30e3)) && DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)<2.",
    
    "reco_weight": "weight_mc*weight_normalise*weight_smtmuSF*weight_pileup*weight_jvt*weight_leptonSF*ChMisId_SF*weight_bTagSF_MV2c10_77",  # weights to be combined into one - will be called weight
    
    "particle_weight": "weight_normalise*weight_mc",
    
    "reco_new_vars": [  # new variables to be created - a list of lists with two entries; 1) name of new variable, 2) how it is constructed
        
        ["calib_jet_pt", "SMT_jet_calibration(jet_pt, SMTmu_jetIdx,1.0)"],    
        [
            "isDilep",
            "fabs(SMTmu_originFlag)==2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isLjets",
            "fabs(SMTmu_originFlag)!=2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFake",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFakeSherpa",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4) && (smtmu_truthmuIndex[SMTmu_smtIdx]<0 || (truthmu_origin[smtmu_truthmuIndex[SMTmu_smtIdx]]!=43))",
        ],
        ["isFakeNonTT", "SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4"],
        ["ejets", "(ejets_2015||ejets_2016)"],
        ["mujets", "(mujets_2015||mujets_2016)"],
        ["doubleTag","(isbtagged_77[SMTmu_jetIdx]>0.5)"],
        ["bTagOther","((isbtagged_77[SMTmu_jetIdx]<0.5)||(isbtagged_77[SMTmu_jetIdx]>0.5 && nBTags_77>=2))"],
        ["weight_ttbar", "PFRew(SMTmu_Bmother_pdgid,mcChannelNumber,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,mcChannelNumber,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,mcChannelNumber,0)"],
        ["ge4j", "(nJets>=4)"],
        ["ge3j", "(nJets>=3)"],
        ["ge2j", "(nJets>=2)"],
        ["ge4j1b", "(nJets>=4&&nBTags_77>=1)"],
        ["ge3j1b", "(nJets>=3&&nBTags_77>=1)"],
        ["ge2j1b", "(nJets>=2&&nBTags_77>=1)"],

        
    ],
        
    "particle_new_vars": [
        ["OS", "1.*(SMTmu_charge*lep_charge<0)+0."],
        ["SS", "1.*(SMTmu_charge*lep_charge>0)+0."],
        [
            "isDilep",
            "fabs(SMTmu_originFlag)==2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isLjets",
            "fabs(SMTmu_originFlag)!=2000 && !((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFake",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4)",
        ],
        [
            "isFakeSherpa",
            "((abs(SMTmu_originFlag)==0 || abs(SMTmu_originFlag)==9999) && SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4) && (smtmu_truthmuIndex[SMTmu_smtIdx]<0 || (truthmu_origin[smtmu_truthmuIndex[SMTmu_smtIdx]]!=43))",
        ],
        ["isFakeNonTT", "SMTmu_origin!=1 && SMTmu_origin!=2 && SMTmu_origin!=4"],
        ["ejets", "(abs(lep_pdgId)==11)"],
        ["mujets", "(abs(lep_pdgId)==11)"],
        ["doubleTag","(smtjet_flav==5)"],
        ["bTagOther","((smtjet_flav!=5)||(smtjet_flav==5 && nBTags_77>=2))"],
        
        ["weight_ttbar", "PFRew(SMTmu_Bmother_pdgid,mcChannelNumber,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,mcChannelNumber,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,mcChannelNumber,0)"],
        
    ],
        
    "functions": [  # any ROOT macros which are required for cuts/weights/new variables
        "macros/PFBRRew.C",
        "macros/PTRel.C",
        "macros/DeltaR.C",
        "macros/LewFuncs.C",
        "macros/Average.C",
        "macros/TopPt.C",
    ],
}
