config = {
    "branches": [
        "jet_pt",
        "lep_pt",
        "Mlmu",
        "runNumber",
        "mcChannelNumber",
        "weight",
        "deltaR",
        "met_met",
        "lepjet_pt",
    ],  # branches common to both particle and reco level samples

    "reco_branches": [
        "SMTmu_jetIdx"
    ],  # branches for only particle or reco - must be list

    "particle_branches": ["smtjet_pt"],

    "reco_cuts": "nSMT>=1 && nBTags_77>=1  && met_met>30e3 && (met_met+mtw)>60e3",  # cuts to apply

    "particle_cuts": "lep_pt/1e3>27 && fabs(lep_eta)<2.5 && (fabs(lep_pdgId)==11 || fabs(lep_pdgId)==13) && nSMT>=1 && nBTags_77>=1  && met_met>30e3 && (met_met+mtw)>60e3",
    
    "reco_weight": "PFRew(SMTmu_Bmother_pdgid,410501,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,410501,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,410501,0)*weight_mc*weight_smtmuSF*weight_pileup*weight_jvt*weight_leptonSF*weight_bTagSF_MV2c10_77",  # weights to be combined into one - will be called weight
    
    "particle_weight": "PFRew(SMTmu_Bmother_pdgid,410501,0)*BRRew(SMTmu_charge,SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,SMTmu_originFlag,410501,0)*CPFRew(SMTmu_Bmother_pdgid,SMTmu_Cmother_pdgid,SMTmu_mother_pdgid,410501,0)*weight_mc",
    
    "reco_new_vars": [  # new variables to be created - a list of lists with two entries; 1) name of new variable, 2) how it is constructed
        ["deltaR", "DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["lepjet_pt", "Mlmu+Mlmu"],
    ],

    "particle_new_vars": [
        ["deltaR", "DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)"],
        ["lepjet_pt", "Mlmu+lep_pt"],
    ],
    
    "functions": [  # any ROOT macros which are required for cuts/weights/new variables
        "macros/PFBRRew.C",
        "macros/PTRel.C",
        "macros/DeltaR.C",
        "macros/LewFuncs.C",
    ],
}

