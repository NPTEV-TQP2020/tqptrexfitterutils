#include "TH1.h"
#include "TFile.h"
#include <iostream>
#include <map>
#include <string>
#include "TString.h"

TString FILEPATH="../MCWeights/MCweights_v241018v5.root";

TFile *m_prodFracFile=0;
std::map<int,TH1*> m_prodfracH;
std::map<int,TH1*> m_prodfracH_Bup;
std::map<int,TH1*> m_prodfracH_Bsup;
std::map<int,TH1*> m_prodfracH_Baryup;
std::map<int,TH1*> m_prodfracH_Bdown;
std::map<int,TH1*> m_prodfracH_Bsdown;
std::map<int,TH1*> m_prodfracH_Barydown;

TFile *m_prodCFracFile=0;
std::map<int,TH1*> m_prodCfracH;
std::map<int,TH1*> m_prodCfracH_Dplusup;
std::map<int,TH1*> m_prodCfracH_D0up;
std::map<int,TH1*> m_prodCfracH_Dsup;
std::map<int,TH1*> m_prodCfracH_CBaryonup;
std::map<int,TH1*> m_prodCfracH_Dplusdown;
std::map<int,TH1*> m_prodCfracH_D0down;
std::map<int,TH1*> m_prodCfracH_Dsdown;
std::map<int,TH1*> m_prodCfracH_CBaryondown;
  
int nWarnings=0;
int nInfo=0;
map<int,int> NwarningsForPdgId;

TFile *m_BRsFile=0;
std::map<int,TH1*> m_BRsH;
std::map<int,TH1*> m_BRsH_bup;
std::map<int,TH1*> m_BRsH_bdown;
std::map<int,TH1*> m_BRsH_btotauup;
std::map<int,TH1*> m_BRsH_btotaudown;
std::map<int,TH1*> m_BRsH_btocCSup;
std::map<int,TH1*> m_BRsH_btocCSdown;
std::map<int,TH1*> m_BRsH_btocWSup;
std::map<int,TH1*> m_BRsH_btocWSdown;
std::map<int,TH1*> m_BRsH_cup;
std::map<int,TH1*> m_BRsH_cdown;

using namespace std;

bool isBottomonium(int pdgid){
	  if(abs(pdgid)==551 || abs(pdgid)==553 || abs(pdgid)==555 || abs(pdgid)==557) return true;
	  if(abs(pdgid)==10551 || abs(pdgid)==10553 || abs(pdgid)==10555 || abs(pdgid)==10557) return true;
	  if(abs(pdgid)==20551 || abs(pdgid)==20553 || abs(pdgid)==20555 || abs(pdgid)==20557) return true;
	  if(abs(pdgid)==100551 || abs(pdgid)==100553 || abs(pdgid)==100555 || abs(pdgid)==100557) return true;
	  if(abs(pdgid)==110551 || abs(pdgid)==110553 || abs(pdgid)==110555 || abs(pdgid)==110557) return true;
	  if(abs(pdgid)==120551 || abs(pdgid)==120553 || abs(pdgid)==120555 || abs(pdgid)==120557) return true;
	  if(abs(pdgid)==130551 || abs(pdgid)==130553 || abs(pdgid)==130555 || abs(pdgid)==130557) return true;
	  if(abs(pdgid)==200551 || abs(pdgid)==200553 || abs(pdgid)==200555 || abs(pdgid)==200557) return true;
	  if(abs(pdgid)==210551 || abs(pdgid)==210553 || abs(pdgid)==210555 || abs(pdgid)==210557) return true;
	  if(abs(pdgid)==220551 || abs(pdgid)==220553 || abs(pdgid)==220555 || abs(pdgid)==220557) return true;
	  if(abs(pdgid)==230551 || abs(pdgid)==230553 || abs(pdgid)==230555 || abs(pdgid)==230557) return true;
	  if(abs(pdgid)==300551 || abs(pdgid)==300553 || abs(pdgid)==300555 || abs(pdgid)==300557) return true;
	  if(abs(pdgid)==9000551 || abs(pdgid)==9000553 || abs(pdgid)==9000555 || abs(pdgid)==9000557) return true;
	  if(abs(pdgid)==9010551 || abs(pdgid)==9010553 || abs(pdgid)==9010555 || abs(pdgid)==9010557) return true;
	  
	  return false;
	  
  }
  
    bool isCharmonium(int pdgId){
	if(abs(pdgId) == 441  ||  abs(pdgId)== 443 ||  abs(pdgId)== 445 || abs(pdgId) == 10441  || abs(pdgId) == 10443  || abs(pdgId) == 20443  || abs(pdgId) == 30443 || abs(pdgId) == 100441  ||  abs(pdgId)== 100443 ||  abs(pdgId)== 100445 || abs(pdgId)== 9000443 || abs(pdgId)== 9010443 || abs(pdgId)== 9020443) return true;
	
	return false;
 }
  
  bool isC(int pdgId) {
    bool isc = false;
    if((fabs(pdgId) > 400 &&  fabs(pdgId) < 500) ||  (fabs(pdgId) > 4000 &&  fabs(pdgId) < 5000) ||  (fabs(pdgId) > 10400 &&  fabs(pdgId) < 10500)||  (fabs(pdgId)
    > 20400 &&  fabs(pdgId) < 20500) ||  (fabs(pdgId) > 100400 &&  fabs(pdgId) < 100500)) isc = true;
    else if (isCharmonium(pdgId)) isc=true;
    else isc = false;
    return isc;
  } 


bool isB(int pdgId) {
    //bool isb = false;
    //if((fabs(pdgId) > 500 &&  fabs(pdgId) < 600) ||  (fabs(pdgId) > 5000 &&  fabs(pdgId) < 6000) ||  (fabs(pdgId) > 10500 &&  fabs(pdgId) < 10600) || 
    //(fabs(pdgId) > 20500 &&  fabs(pdgId) < 20600)) isb = true;
    //else isb = false;
    //return isb;
    
    int pid=abs(pdgId);
    if(pid>500 && pid <600) return true;
    if(pid>5000 && pid <6000) return true;
    if(pid>10500 && pid <10600) return true;
    if(pid>20500 && pid <20600) return true;
    if(pid>500 && pid <600) return true;
    if(pid==100551) return true;
    if(pid==110551) return true;
    if(pid==200551) return true;
    if(pid==210551) return true;
    if(pid==30553) return true;
    if(pid==100553) return true;
    if(pid==110553) return true;
    if(pid==120553) return true;
    if(pid==130553) return true;
    if(pid==200553) return true;
    if(pid==210553) return true;
    if(pid==220553) return true;
    if(pid==300553) return true;
    if(pid==9000553) return true;
    if(pid==9010553) return true;
    if(pid==100555) return true;
    if(pid==110555) return true;
    if(pid==120555) return true;
    if(pid==200555) return true;
    if(pid==557) return true;
    if(pid==100557) return true;
    
    return false;
  }
  
bool isDplus(int pdgid)
{
	if(abs(pdgid)==411) return true;
	return false;
} 
bool isD0(int pdgid)
{
	if(abs(pdgid)==421) return true;
	return false;
} 
bool isDs(int pdgid)
{
	if(abs(pdgid)==431) return true;
	return false;
} 
bool isLambdaC(int pdgid)
{
	if(abs(pdgid)==4122) return true;
	return false;
} 
bool isBplus(int pdgid) {
  if(abs(pdgid) == 521 || abs(pdgid) == 523 || abs(pdgid) == 525 || abs(pdgid) == 10521|| abs(pdgid) == 10523|| abs(pdgid) == 10525 || abs(pdgid) == 20521|| abs(pdgid) == 20523|| abs(pdgid) == 20525) return true;
  else return false;
}

bool isB0(int pdgid) {
  if(abs(pdgid) == 511 || abs(pdgid) == 513 || abs(pdgid) == 515 || abs(pdgid) == 10511 || abs(pdgid) == 10513 || abs(pdgid) == 10515 || abs(pdgid) == 20511 || abs(pdgid) == 20513 || abs(pdgid) == 20515) return true;
  else return false;
}

bool isB0s(int pdgid) {
  if(abs(pdgid) == 531 || abs(pdgid) == 533 || abs(pdgid) == 535 || abs(pdgid) == 10531 || abs(pdgid) == 10533 || abs(pdgid) == 10535  || abs(pdgid) == 20531 || abs(pdgid) == 20533 || abs(pdgid) == 20535) return true;
  else return false;
}

bool isBc(int pdgid) {
  if(abs(pdgid) == 541 || abs(pdgid) == 543 || abs(pdgid) == 545 || abs(pdgid) == 10541 || abs(pdgid) == 10543 || abs(pdgid) == 10545  || abs(pdgid) == 20541 || abs(pdgid) == 20543 || abs(pdgid) == 20545) return true;
  else return false;
}

bool isBbary(int pdgid) {
  if(fabs(pdgid) > 5000 && fabs(pdgid) < 6000) return true;
  else return false;
}

void PFBRRew()
{
	
}
bool isKnownPP8Sample(int mcChannelNumber)
{
	if(mcChannelNumber==411169) return true;
	if(mcChannelNumber==410501) return true;
	if(mcChannelNumber>=411235 && mcChannelNumber<= 411239) return true;
	if(mcChannelNumber>=411241 && mcChannelNumber<= 411247) return true;
	if(mcChannelNumber==411257) return true;
	if(mcChannelNumber==411258) return true; //actually aMCatNLOP8, but ok
	if(mcChannelNumber>=411249 && mcChannelNumber<= 411256) return true;
	if(mcChannelNumber==411259) return true;
	if(mcChannelNumber==411260) return true;
	if(mcChannelNumber==411248) return true;
	if(mcChannelNumber>=411261 && mcChannelNumber<= 411268) return true;
	if(mcChannelNumber==411279) return true;
	if(mcChannelNumber==411280) return true;
	if(mcChannelNumber==411281) return true;
	return false;
}
int getCorrespondingDSID(int mcChannelNumber)
{
	int dsId=410501;
	if(mcChannelNumber==410525)//PH7.0.4
	{
		 if(nInfo<1)
		 {
			 std::cout<<"INFO: DSID="<<mcChannelNumber<<" PF/BR rwt will use PH7.0.4 values "<<std::endl;
			 nInfo++;
		 }
		 dsId=410525; 
	}
	else if(mcChannelNumber==999525 || mcChannelNumber==411168) //PH7.1.3 angular
	{
		 if(nInfo<1)
		 {
			 std::cout<<"INFO: DSID="<<mcChannelNumber<<" PF/BR rwt will use PH7.1.3 (angular) values "<<std::endl;
			 nInfo++;
		 }
		 dsId=999525; 
	}
	else if(mcChannelNumber==411269) //PH7.1.3 dipole
	{
		if(nInfo<1)
		 {
			 std::cout<<"INFO: DSID="<<mcChannelNumber<<" PF/BR rwt will use PH7.1.3 (dipole) values "<<std::endl;
			 nInfo++;
		 }
		 dsId=411269; 
	}
	else if(mcChannelNumber==411278) //PP6
	{
		if(nInfo<1)
		 {
			 std::cout<<"INFO: DSID="<<mcChannelNumber<<" PF/BR rwt will use PP6 values "<<std::endl;
			 nInfo++;
		 }
		 dsId=411278; 
	}
	else if(mcChannelNumber==411240) //PP8 pythia 8.240
	{
		if(nInfo<1)
		 {
			 std::cout<<"INFO: DSID="<<mcChannelNumber<<" PF/BR rwt will use PP8.240 values "<<std::endl;
			 nInfo++;
		 }
		 dsId=411240; 
	}
	else if(isKnownPP8Sample(mcChannelNumber))
	{
		if(nInfo<1)
		 {
			 std::cout<<"INFO: DSID="<<mcChannelNumber<<" PF/BR rwt will use PP8 values "<<std::endl;
			 nInfo++;
		 }
		 dsId=410501; //known PP8 cases
	}
	else
	{
		if(nWarnings<10)
		{
			std::cout<<"WARNING, DSID "<<mcChannelNumber<<" not recognised, PF/BR rwt will assume this is a PP8 sample, check if this is correct "<<std::endl;
			nWarnings++;
		}
		dsId=410501;//default is PP8;
	}
	
	
	return dsId;
}

float CPFRew(int SMTmu_Bmother_pdgid=0, int SMTmu_Cmother_pdgid=0, int SMTmu_mother_pdgid=0, int mcChannelNumber=410501, int syst=0)
{
	int dsId=getCorrespondingDSID(mcChannelNumber);
	
	if(!m_prodCFracFile)
	{
		m_prodCFracFile =new TFile(FILEPATH);
		if(!m_prodCFracFile)
		{
			//std::cout<<"ERROR, CPFrew file not found in PFRew.C"<<std::endl;
		}
	}
	
	if(!m_prodCfracH[dsId])
	{		
		m_prodCfracH[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d",dsId));
		m_prodCfracH_Dplusup[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_Dplusup",dsId));
		m_prodCfracH_D0up[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_D0up",dsId));
		m_prodCfracH_Dsup[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_Dsup",dsId));
		m_prodCfracH_CBaryonup[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_CBaryonup",dsId));
		m_prodCfracH_Dplusdown[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_Dplusdown",dsId));
		m_prodCfracH_D0down[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_D0down",dsId));
		m_prodCfracH_Dsdown[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_Dsdown",dsId));
		m_prodCfracH_CBaryondown[dsId]         = (TH1*)m_prodCFracFile->Get(Form("h_prodCfrac_SF_%d_CBaryondown",dsId));
	     
        if(!m_prodCfracH[dsId])
        {
			//  std::cout<<"ERROR, in PFrew histo "<<Form("h_prodfrac_SF_%d",dsId)<<" not found "<<std::endl;
		}
	}
	TH1* h = 0x0;
    if(syst==0)       h = m_prodCfracH[dsId];
    else if(syst==1)      h = m_prodCfracH_Dplusup[dsId];
    else if(syst==2)     h = m_prodCfracH_D0up[dsId];
    else if(syst==3)   h = m_prodCfracH_Dsup[dsId];
    else if(syst==4)    h = m_prodCfracH_CBaryonup[dsId];
    else if(syst==5)      h = m_prodCfracH_Dplusdown[dsId];
    else if(syst==6)     h = m_prodCfracH_D0down[dsId];
    else if(syst==7)   h = m_prodCfracH_Dsdown[dsId];
    else if(syst==8)    h = m_prodCfracH_CBaryondown[dsId];
    else{
		//std::cout<<"ERROR, in PFrew, syst="<<syst<<" not known"<<std::endl;
	}
    //
    if(h==0x0){
      //std::cout << "PFRew::ERROR: histogram not found. Returning 1. dsId="<<dsId<<" syst="<<syst<<" "<< std::endl;
			// m_prodCFracFile->Close();
			// delete m_prodCFracFile;
      return 1.;
    }
    
    float w = 1.;
    
    if(SMTmu_Cmother_pdgid==0) return 1.; //the muon is not from c
	if(SMTmu_Bmother_pdgid!=0) return 1.; //we don't reweight B->C
	
	int momid=0;
	
	if(isC(SMTmu_mother_pdgid)) momid=SMTmu_mother_pdgid; //this is a fix because due to a bug when filling the ntuples, we have only the pdgId of the first C mother for C hadrons, and we would be missing cases with excited mesons; when we can, we then use the final C mother. This doesn't work actually for C->tau->mu, which is very small. I'm adding some way of handling at least part of the cases afterwards
	else if(abs(SMTmu_Cmother_pdgid)==423) momid=421;
	else momid=SMTmu_Cmother_pdgid;
	
	static int NEV=0;
	static int NEVBLAH=0;
	NEV++;
	
	std::map<int,int>::iterator it=NwarningsForPdgId.find(momid);
    if(it==NwarningsForPdgId.end()) NwarningsForPdgId[momid]=0;
	
	if(isDplus(momid)) { w = h->GetBinContent(1); } //D+ or D-
	else if(isD0(momid)) { w = h->GetBinContent(2); } //D0
	else if(isDs(momid)) { w = h->GetBinContent(3); } //Ds
	else if(isLambdaC(momid)) { w = h->GetBinContent(4); } //D+ or D-
    else if(NwarningsForPdgId[momid]<10){
		std::cout<<"WARNING, PROD FRAC weight not found for pdgId="<<momid<<" muon mother is "<<SMTmu_mother_pdgid<<std::endl;
		NwarningsForPdgId[momid]++;
		if(NwarningsForPdgId[momid]>=10) std::cout<<"---> suppressing warnings for mother pdgId="<<momid<<" in CPFRew"<<std::endl;
		NEVBLAH++;
		//std::cout<<"NEVBLAH fraction="<<((double)NEVBLAH/(double)NEV)<<std::endl;
		}
    return w;
}

float PFRew(int SMTmu_Bmother_pdgid=511, int mcChannelNumber=410501, int syst=0)
{

	int dsId=getCorrespondingDSID(mcChannelNumber);
	
	if(!m_prodFracFile)
	{
		m_prodFracFile =new TFile(FILEPATH);
		if(!m_prodFracFile)
		{
			//std::cout<<"ERROR, PFrew file not found in PFRew.C"<<std::endl;
		}
	}
	
	if(!m_prodfracH[dsId])
	{		
		m_prodfracH[dsId]         = (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d",dsId));
        m_prodfracH_Bup[dsId]     = (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d_Bup",dsId));
        m_prodfracH_Bsup[dsId]    = (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d_Bsup",dsId));
        m_prodfracH_Baryup[dsId]  = (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d_Baryup",dsId));
        m_prodfracH_Bdown[dsId]   = (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d_Bdown",dsId));
        m_prodfracH_Bsdown[dsId]  = (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d_Bsdown",dsId));
        m_prodfracH_Barydown[dsId]= (TH1*)m_prodFracFile->Get(Form("h_prodfrac_SF_%d_Barydown",dsId));
        
        if(!m_prodfracH[dsId])
        {
			// std::cout<<"ERROR, in PFrew histo "<<Form("h_prodfrac_SF_%d",dsId)<<" not found "<<std::endl;
		}
	}
	TH1* h = 0x0;
    if(syst==0)       h = m_prodfracH[dsId];
    else if(syst==1)      h = m_prodfracH_Bup[dsId];
    else if(syst==2)     h = m_prodfracH_Bsup[dsId];
    else if(syst==3)   h = m_prodfracH_Baryup[dsId];
    else if(syst==4)    h = m_prodfracH_Bdown[dsId];
    else if(syst==5)   h = m_prodfracH_Bsdown[dsId];
    else if(syst==6) h = m_prodfracH_Barydown[dsId];
    else{
		//std::cout<<"ERROR, in PFrew, syst="<<syst<<" not known"<<std::endl;
	}
    //
    if(h==0x0){
      //std::cout << "PFRew::ERROR: histogram not found. Returning 1. dsId="<<dsId<<" syst="<<syst<<" "<< std::endl;
      return 1.;
    }
    
    float w = 1.;
  
    int mother_pdgid=SMTmu_Bmother_pdgid;
    if(mother_pdgid==0) return 1.; //we only reweight things coming from a B...
    else if(!isB(mother_pdgid))
    {
	//   std::cout<<"WARNING, PFRew called for a pdgId not corresponding to a B-hadron!"<<std::endl;
	  return w;
    }
    
    std::map<int,int>::iterator it=NwarningsForPdgId.find(mother_pdgid);
    if(it==NwarningsForPdgId.end()) NwarningsForPdgId[mother_pdgid]=0;
	
    if(isBplus(mother_pdgid)) { w = h->GetBinContent(1); } //B+
    else if(isB0(mother_pdgid)) { w = h->GetBinContent(2); } //B0
    else if(isB0s(mother_pdgid)) { w = h->GetBinContent(3); } //Bs
    else if(isBc(mother_pdgid)) { w = h->GetBinContent(4); } //Bc
    else if(isBbary(mother_pdgid)){ w = h->GetBinContent(5); }
    else if(NwarningsForPdgId[mother_pdgid]<10){
		// std::cout<<"WARNING, PROD FRAC weight not found for pdgId="<<mother_pdgid<<std::endl;
		NwarningsForPdgId[mother_pdgid]++;
		if(NwarningsForPdgId[mother_pdgid]>=10) std::cout<<"---> suppressing warnings for mother pdgId="<<mother_pdgid<<" in PFRew"<<std::endl;
		}
  
    return w;
}

float BRRew(int SMTmu_charge=1, int SMTmu_Bmother_pdgid=0, int SMTmu_Cmother_pdgid=0, int SMTmu_mother_pdgid=0, int SMTmu_originFlag=0, int mcChannelNumber=410501, int syst=0)
{
	if(abs(SMTmu_charge)!=1) return 1; //no SMT in the event
	int dsId=getCorrespondingDSID(mcChannelNumber);
	
	if(!m_BRsFile)
	{
		m_BRsFile =new TFile(FILEPATH);
		if(!m_BRsFile)
		{
			// std::cout<<"ERROR, BRrew file not found in BRRew.C"<<std::endl;
		}
	}
	
	if(!m_BRsH[dsId])
	{	
		m_BRsH[dsId]          = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d",dsId));
        m_BRsH_bup[dsId]      = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_bup",dsId));
        m_BRsH_bdown[dsId]    = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_bdown",dsId));
        m_BRsH_btotauup[dsId]      = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_btauup",dsId));
        m_BRsH_btotaudown[dsId]    = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_btaudown",dsId));
        m_BRsH_btocCSup[dsId]   = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_btocup",dsId));
        m_BRsH_btocCSdown[dsId] = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_btocdown",dsId));
        m_BRsH_btocWSup[dsId]   = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_btoc2up",dsId));
        m_BRsH_btocWSdown[dsId] = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_btoc2down",dsId));
        m_BRsH_cup[dsId]      = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_cup",dsId));
        m_BRsH_cdown[dsId]    = (TH1*)m_BRsFile->Get(Form("h_BRs_SF_%d_cdown",dsId));	
        
        if(!m_BRsH[dsId])
        {
			//  std::cout<<"ERROR, in BRrew histo not found "<<std::endl;
		}
	}
	TH1* h = 0x0;
	if(syst==0) h=m_BRsH[dsId];
	else if(syst==1) h=m_BRsH_bup[dsId];
	else if(syst==2) h=m_BRsH_bdown[dsId];
	else if(syst==3) h=m_BRsH_btotauup[dsId];
	else if(syst==4) h=m_BRsH_btotaudown[dsId];
	else if(syst==5) h=m_BRsH_btocCSup[dsId];
	else if(syst==6) h=m_BRsH_btocCSdown[dsId];
	else if(syst==7) h=m_BRsH_btocWSup[dsId];
	else if(syst==8) h=m_BRsH_btocWSdown[dsId];
	else if(syst==9) h=m_BRsH_cup[dsId];
	else if(syst==10) h=m_BRsH_cdown[dsId];
    else{
		//std::cout<<"ERROR, in BRrew, syst="<<syst<<" not known"<<std::endl;
	}
    
    if(h==0x0){
      //std::cout << "BRRew::ERROR: histogram not found. Returning 1. dsId="<<dsId<<" syst="<<syst<<" "<< std::endl;
      return 1.;
    }
    
    float w = 1.;
    
    if(SMTmu_mother_pdgid==0) return 1.;
    if(isBottomonium(SMTmu_Bmother_pdgid)) return 1.; //we don't reweight cases with bottomonium
    if(!isC(SMTmu_mother_pdgid) && !isB(SMTmu_mother_pdgid) && abs(SMTmu_mother_pdgid)!=15) return 1.; // we don't reweight muons passing through light hadrons
    
    if(isB(SMTmu_Bmother_pdgid) && !isC(SMTmu_Cmother_pdgid) && abs(SMTmu_mother_pdgid)!=15 && isB(SMTmu_mother_pdgid)  && !isBottomonium(SMTmu_mother_pdgid))
    {
		w = h->GetBinContent(1); 	
	}
	else if(isB(SMTmu_Bmother_pdgid) && !isC(SMTmu_Cmother_pdgid) && abs(SMTmu_mother_pdgid)==15)
	{
		w = h->GetBinContent(2); 
	}
	else if(isB(SMTmu_Bmother_pdgid) && isC(SMTmu_Cmother_pdgid) && abs(SMTmu_mother_pdgid)!=15  && isC(SMTmu_mother_pdgid)  && (isB0(SMTmu_Bmother_pdgid)||isB0s(SMTmu_Bmother_pdgid)||isBplus(SMTmu_Bmother_pdgid) || isBbary(SMTmu_Bmother_pdgid)))
	{
		
		if(isBbary(SMTmu_Bmother_pdgid))
		{
			if(SMTmu_Bmother_pdgid>0 && SMTmu_charge>0) w = h->GetBinContent(3); //correct sign
			if(SMTmu_Bmother_pdgid>0 && SMTmu_charge<0) w = h->GetBinContent(4); //wrong sign
			if(SMTmu_Bmother_pdgid<0 && SMTmu_charge>0) w = h->GetBinContent(4); //wrong sign
			if(SMTmu_Bmother_pdgid<0 && SMTmu_charge<0) w = h->GetBinContent(3); //correct sign
			
		}
		else{
			if(SMTmu_Bmother_pdgid<0 && SMTmu_charge>0) w = h->GetBinContent(3); //correct sign
			if(SMTmu_Bmother_pdgid<0 && SMTmu_charge<0) w = h->GetBinContent(4); //wrong sign
			if(SMTmu_Bmother_pdgid>0 && SMTmu_charge>0) w = h->GetBinContent(4); //wrong sign
			if(SMTmu_Bmother_pdgid>0 && SMTmu_charge<0) w = h->GetBinContent(3); //correct sign
		}
	}
	else if(!isB(SMTmu_Bmother_pdgid) && isC(SMTmu_Cmother_pdgid) && abs(SMTmu_mother_pdgid)!=15 && isC(SMTmu_mother_pdgid) )
	{
		w = h->GetBinContent(5); 
	}
	
    return w;
}


