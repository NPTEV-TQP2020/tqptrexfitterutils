# script to run / submit TRExFitter jobs

config="config/nptev_V18_fit.config"

action=n

regions=""
regions+=" Mlmu_optiSel_btagAny"

# systematics="all"
systematics="none"

addOpt=""

bootstrap="no"
# bootstrap="yes"

# -----------------------

bootstrapMin=-1
bootstrapMax=-1

if [[ ${bootstrap} == "yes" ]]
then
  bootstrapMax=99
fi


# to launch jobs
for region in ${regions}
do
  for i in $(seq ${bootstrapMin} ${bootstrapMax})
  do
    for syst in ${systematics}
    do
      options="Regions=${region}"
      options+=${addOpt}
      if [[ $syst != "all" ]]
      then
        options+=":Systematics=${syst}"
      fi
      if [[ $i != -1 ]]
      then
        options+=":BootstrapIdx=${i}"
      fi
      export OPT="${action} ${config} ${options}"
      echo "--------------------------------------------------------------------------------------------------"
      echo "Sending job executing this line:    trex-fitter ${OPT}"
#       qsubl myFit.job -V
      qsub2 myFit.job -V
      echo "--------------------------------------------------------------------------------------------------"
    done
  done
done
