#include "pdg.h"
#include "TH1D.h"
#include "TString.h"

#include <map>
#include <iostream>

using namespace std;

#ifndef __SAMPLE__
#define __SAMPLE__

class Sample{
	
	public:
		Sample(TString dsid)
		{
				//cout<<"Calling Sample constructor "<<dsid<<endl;
				m_dsid=dsid;
			    InitPFHistos();
		        InitCPFHistos();
		        InitBRHistos();
		        m_frac_0=1.;
		        m_frac_1=1.;
		        m_frac_2=1.;
		        m_frac_3=1.;
		        m_frac_4=1.;
		        m_br_0=1.;
		        m_br_1=1.;
		        m_br_2=1.;
		        m_br_3=1.;
		        m_br_4=1.;
		        m_Cfrac_0=1.;
		        m_Cfrac_1=1.;
		        m_Cfrac_2=1.;
		        m_Cfrac_4=1.;
		        //cout<<"---end of constructor"<<endl;
		        
		};
		
		void setPF(double Bp, double B0, double Bs,  double Bary)	{m_frac_0=Bp; m_frac_1=B0; m_frac_2=Bs; m_frac_4=Bary;}
		void setBR(double bmu, double btaumu, double bcmuCS, double bcmuWS, double cmu) { m_br_0=bmu;  m_br_1=btaumu;  m_br_2=bcmuCS;  m_br_3=bcmuWS;  m_br_4=cmu;}
		void setCPF(double Dp, double D0, double Ds,  double Lambdac)	{m_Cfrac_0=Dp; m_Cfrac_1=D0; m_Cfrac_2=Ds; m_Cfrac_4=Lambdac;}
		
		void calcHistos();
		void writeToFile(TString infilename);
		
	private:
		TString m_dsid;
		
		Double_t m_frac_0,m_frac_1,m_frac_2,m_frac_3,m_frac_4;	
		Double_t m_br_0,m_br_1,m_br_2,m_br_3,m_br_4;
		Double_t m_Cfrac_0,m_Cfrac_1,m_Cfrac_2,m_Cfrac_4;
		
	map<TString,TH1D*> m_histos;
		
		void InitPFHistos();
		void InitCPFHistos();
		void InitBRHistos();
		
};

void Sample::calcHistos()
{
	//cout<<"calling Sample::calcHistos() "<<m_dsid<<endl;
	///////////////B-PF////////////////////////
	
	Double_t w0_prodfrac = m_frac_0/PDG_0;
	Double_t w1_prodfrac = m_frac_1/PDG_1;
	Double_t w2_prodfrac = m_frac_2/PDG_2;
	Double_t w3_prodfrac = 1.;
	Double_t w4_prodfrac = m_frac_4/PDG_4;
	
	Double_t w0_prodfrac_a = m_frac_0/PDG_0_a;
	Double_t w1_prodfrac_a = m_frac_1/PDG_1_a;
	Double_t w2_prodfrac_a = m_frac_2/PDG_2_a;
	Double_t w3_prodfrac_a = 1.;
	Double_t w4_prodfrac_a = m_frac_4/PDG_4_a;

	Double_t w0_prodfrac_b = m_frac_0/PDG_0_b;
	Double_t w1_prodfrac_b = m_frac_1/PDG_1_b;
	Double_t w2_prodfrac_b = m_frac_2/PDG_2_b;
	Double_t w3_prodfrac_b = 1.;
	Double_t w4_prodfrac_b = m_frac_4/PDG_4_b;

	Double_t w0_prodfrac_c = m_frac_0/PDG_0_c;
	Double_t w1_prodfrac_c = m_frac_1/PDG_1_c;
	Double_t w2_prodfrac_c = m_frac_2/PDG_2_c;
	Double_t w3_prodfrac_c = 1.;
	Double_t w4_prodfrac_c = m_frac_4/PDG_4_c;

	Double_t w0_prodfrac_d = m_frac_0/PDG_0_d;
	Double_t w1_prodfrac_d = m_frac_1/PDG_1_d;
	Double_t w2_prodfrac_d = m_frac_2/PDG_2_d;
	Double_t w3_prodfrac_d = 1.;
	Double_t w4_prodfrac_d = m_frac_4/PDG_4_d;

	Double_t w0_prodfrac_e = m_frac_0/PDG_0_e;
	Double_t w1_prodfrac_e = m_frac_1/PDG_1_e;
	Double_t w2_prodfrac_e = m_frac_2/PDG_2_e;
	Double_t w3_prodfrac_e = 1.;
	Double_t w4_prodfrac_e = m_frac_4/PDG_4_e;

	Double_t w0_prodfrac_f = m_frac_0/PDG_0_f;
	Double_t w1_prodfrac_f = m_frac_1/PDG_1_f;
	Double_t w2_prodfrac_f = m_frac_2/PDG_2_f;
	Double_t w3_prodfrac_f = 1.;
	Double_t w4_prodfrac_f = m_frac_4/PDG_4_f;
	
	m_histos["prodfrac_SF"]->Fill(1.,1./w0_prodfrac);
	m_histos["prodfrac_SF"]->Fill(2.,1./w1_prodfrac);
	m_histos["prodfrac_SF"]->Fill(3.,1./w2_prodfrac);
	m_histos["prodfrac_SF"]->Fill(4.,1./w3_prodfrac);
	m_histos["prodfrac_SF"]->Fill(5.,1./w4_prodfrac);
	
	//cout<<m_dsid<<" m_frac_0="<<m_frac_0<<" PDG_0="<<PDG_0<<" "<<w0_prodfrac<<" "<<m_histos["prodfrac_SF"]->GetBinContent(1)<<endl;

	m_histos["prodfrac_SF_Bup"]->Fill(1.,1./w0_prodfrac_a);
	m_histos["prodfrac_SF_Bup"]->Fill(2.,1./w1_prodfrac_a);
	m_histos["prodfrac_SF_Bup"]->Fill(3.,1./w2_prodfrac_a);
	m_histos["prodfrac_SF_Bup"]->Fill(4.,1./w3_prodfrac_a);
	m_histos["prodfrac_SF_Bup"]->Fill(5.,1./w4_prodfrac_a);

	m_histos["prodfrac_SF_Bdown"]->Fill(1.,1./w0_prodfrac_b);
	m_histos["prodfrac_SF_Bdown"]->Fill(2.,1./w1_prodfrac_b);
	m_histos["prodfrac_SF_Bdown"]->Fill(3.,1./w2_prodfrac_b);
	m_histos["prodfrac_SF_Bdown"]->Fill(4.,1./w3_prodfrac_b);
	m_histos["prodfrac_SF_Bdown"]->Fill(5.,1./w4_prodfrac_b);

	m_histos["prodfrac_SF_Bsup"]->Fill(1.,1./w0_prodfrac_c);
	m_histos["prodfrac_SF_Bsup"]->Fill(2.,1./w1_prodfrac_c);
	m_histos["prodfrac_SF_Bsup"]->Fill(3.,1./w2_prodfrac_c);
	m_histos["prodfrac_SF_Bsup"]->Fill(4.,1./w3_prodfrac_c);
	m_histos["prodfrac_SF_Bsup"]->Fill(5.,1./w4_prodfrac_c);

	m_histos["prodfrac_SF_Bsdown"]->Fill(1.,1./w0_prodfrac_d);
	m_histos["prodfrac_SF_Bsdown"]->Fill(2.,1./w1_prodfrac_d);
	m_histos["prodfrac_SF_Bsdown"]->Fill(3.,1./w2_prodfrac_d);
	m_histos["prodfrac_SF_Bsdown"]->Fill(4.,1./w3_prodfrac_d);
	m_histos["prodfrac_SF_Bsdown"]->Fill(5.,1./w4_prodfrac_d);

	m_histos["prodfrac_SF_Baryup"]->Fill(1.,1./w0_prodfrac_e);
	m_histos["prodfrac_SF_Baryup"]->Fill(2.,1./w1_prodfrac_e);
	m_histos["prodfrac_SF_Baryup"]->Fill(3.,1./w2_prodfrac_e);
	m_histos["prodfrac_SF_Baryup"]->Fill(4.,1./w3_prodfrac_e);
	m_histos["prodfrac_SF_Baryup"]->Fill(5.,1./w4_prodfrac_e);

	m_histos["prodfrac_SF_Barydown"]->Fill(1.,1./w0_prodfrac_f);
	m_histos["prodfrac_SF_Barydown"]->Fill(2.,1./w1_prodfrac_f);
	m_histos["prodfrac_SF_Barydown"]->Fill(3.,1./w2_prodfrac_f);
	m_histos["prodfrac_SF_Barydown"]->Fill(4.,1./w3_prodfrac_f);
	m_histos["prodfrac_SF_Barydown"]->Fill(5.,1./w4_prodfrac_f);
	
	///////////////C-PF////////////////////////

	Double_t w0_prodCfrac = m_Cfrac_0/CPDG_411;
	Double_t w1_prodCfrac = m_Cfrac_1/CPDG_421;
	Double_t w2_prodCfrac = m_Cfrac_2/CPDG_431;
	Double_t w3_prodCfrac = m_Cfrac_4/CPDG_4122;
	
	Double_t w0_prodCfrac_a = m_Cfrac_0/CPDG_411_a;
	Double_t w1_prodCfrac_a = m_Cfrac_1/CPDG_421_a;
	Double_t w2_prodCfrac_a = m_Cfrac_2/CPDG_431_a;
	Double_t w3_prodCfrac_a = m_Cfrac_4/CPDG_4122_a;
	
	Double_t w0_prodCfrac_b = m_Cfrac_0/CPDG_411_b;
	Double_t w1_prodCfrac_b = m_Cfrac_1/CPDG_421_b;
	Double_t w2_prodCfrac_b = m_Cfrac_2/CPDG_431_b;
	Double_t w3_prodCfrac_b = m_Cfrac_4/CPDG_4122_b;
	
	Double_t w0_prodCfrac_c = m_Cfrac_0/CPDG_411_c;
	Double_t w1_prodCfrac_c = m_Cfrac_1/CPDG_421_c;
	Double_t w2_prodCfrac_c = m_Cfrac_2/CPDG_431_c;
	Double_t w3_prodCfrac_c = m_Cfrac_4/CPDG_4122_c;
	
	Double_t w0_prodCfrac_d = m_Cfrac_0/CPDG_411_d;
	Double_t w1_prodCfrac_d = m_Cfrac_1/CPDG_421_d;
	Double_t w2_prodCfrac_d = m_Cfrac_2/CPDG_431_d;
	Double_t w3_prodCfrac_d = m_Cfrac_4/CPDG_4122_d;
	
	Double_t w0_prodCfrac_e = m_Cfrac_0/CPDG_411_e;
	Double_t w1_prodCfrac_e = m_Cfrac_1/CPDG_421_e;
	Double_t w2_prodCfrac_e = m_Cfrac_2/CPDG_431_e;
	Double_t w3_prodCfrac_e = m_Cfrac_4/CPDG_4122_e;
	
	Double_t w0_prodCfrac_f = m_Cfrac_0/CPDG_411_f;
	Double_t w1_prodCfrac_f = m_Cfrac_1/CPDG_421_f;
	Double_t w2_prodCfrac_f = m_Cfrac_2/CPDG_431_f;
	Double_t w3_prodCfrac_f = m_Cfrac_4/CPDG_4122_f;
	
	Double_t w0_prodCfrac_g = m_Cfrac_0/CPDG_411_g;
	Double_t w1_prodCfrac_g = m_Cfrac_1/CPDG_421_g;
	Double_t w2_prodCfrac_g = m_Cfrac_2/CPDG_431_g;
	Double_t w3_prodCfrac_g = m_Cfrac_4/CPDG_4122_g;
	
	Double_t w0_prodCfrac_h = m_Cfrac_0/CPDG_411_h;
	Double_t w1_prodCfrac_h = m_Cfrac_1/CPDG_421_h;
	Double_t w2_prodCfrac_h = m_Cfrac_2/CPDG_431_h;
	Double_t w3_prodCfrac_h = m_Cfrac_4/CPDG_4122_h;
	
	m_histos["prodCfrac_SF"]->Fill(1.,1./w0_prodCfrac);
	m_histos["prodCfrac_SF"]->Fill(2.,1./w1_prodCfrac);
	m_histos["prodCfrac_SF"]->Fill(3.,1./w2_prodCfrac);
	m_histos["prodCfrac_SF"]->Fill(4.,1./w3_prodCfrac);

	m_histos["prodCfrac_SF_Dplusup"]->Fill(1.,1./w0_prodCfrac_a);
	m_histos["prodCfrac_SF_Dplusup"]->Fill(2.,1./w1_prodCfrac_a);
	m_histos["prodCfrac_SF_Dplusup"]->Fill(3.,1./w2_prodCfrac_a);
	m_histos["prodCfrac_SF_Dplusup"]->Fill(4.,1./w3_prodCfrac_a);

	m_histos["prodCfrac_SF_Dplusdown"]->Fill(1.,1./w0_prodCfrac_b);
	m_histos["prodCfrac_SF_Dplusdown"]->Fill(2.,1./w1_prodCfrac_b);
	m_histos["prodCfrac_SF_Dplusdown"]->Fill(3.,1./w2_prodCfrac_b);
	m_histos["prodCfrac_SF_Dplusdown"]->Fill(4.,1./w3_prodCfrac_b);

	m_histos["prodCfrac_SF_D0up"]->Fill(1.,1./w0_prodCfrac_c);
	m_histos["prodCfrac_SF_D0up"]->Fill(2.,1./w1_prodCfrac_c);
	m_histos["prodCfrac_SF_D0up"]->Fill(3.,1./w2_prodCfrac_c);
	m_histos["prodCfrac_SF_D0up"]->Fill(4.,1./w3_prodCfrac_c);

	m_histos["prodCfrac_SF_D0down"]->Fill(1.,1./w0_prodCfrac_d);
	m_histos["prodCfrac_SF_D0down"]->Fill(2.,1./w1_prodCfrac_d);
	m_histos["prodCfrac_SF_D0down"]->Fill(3.,1./w2_prodCfrac_d);
	m_histos["prodCfrac_SF_D0down"]->Fill(4.,1./w3_prodCfrac_d);

	m_histos["prodCfrac_SF_Dsup"]->Fill(1.,1./w0_prodCfrac_e);
	m_histos["prodCfrac_SF_Dsup"]->Fill(2.,1./w1_prodCfrac_e);
	m_histos["prodCfrac_SF_Dsup"]->Fill(3.,1./w2_prodCfrac_e);
	m_histos["prodCfrac_SF_Dsup"]->Fill(4.,1./w3_prodCfrac_e);

	m_histos["prodCfrac_SF_Dsdown"]->Fill(1.,1./w0_prodCfrac_f);
	m_histos["prodCfrac_SF_Dsdown"]->Fill(2.,1./w1_prodCfrac_f);
	m_histos["prodCfrac_SF_Dsdown"]->Fill(3.,1./w2_prodCfrac_f);
	m_histos["prodCfrac_SF_Dsdown"]->Fill(4.,1./w3_prodCfrac_f);

	m_histos["prodCfrac_SF_CBaryonup"]->Fill(1.,1./w0_prodCfrac_g);
	m_histos["prodCfrac_SF_CBaryonup"]->Fill(2.,1./w1_prodCfrac_g);
	m_histos["prodCfrac_SF_CBaryonup"]->Fill(3.,1./w2_prodCfrac_g);
	m_histos["prodCfrac_SF_CBaryonup"]->Fill(4.,1./w3_prodCfrac_g);

	m_histos["prodCfrac_SF_CBaryondown"]->Fill(1.,1./w0_prodCfrac_h);
	m_histos["prodCfrac_SF_CBaryondown"]->Fill(2.,1./w1_prodCfrac_h);
	m_histos["prodCfrac_SF_CBaryondown"]->Fill(3.,1./w2_prodCfrac_h);
	m_histos["prodCfrac_SF_CBaryondown"]->Fill(4.,1./w3_prodCfrac_h);

	
	///////////////BR////////////////////////
	
	Double_t w0_BRs = m_br_0/PDG_BRs_0;
	Double_t w1_BRs = m_br_1/PDG_BRs_1;
	Double_t w2_BRs = m_br_2/PDG_BRs_2;
	Double_t w3_BRs = m_br_3/PDG_BRs_3;
	Double_t w4_BRs = m_br_4/PDG_BRs_4;
	
	Double_t w0_BRs_a = m_br_0/PDG_BRs_0_a;
	Double_t w1_BRs_a = m_br_1/PDG_BRs_1_a;
	Double_t w2_BRs_a = m_br_2/PDG_BRs_2_a;
	Double_t w3_BRs_a = m_br_3/PDG_BRs_3_a;
	Double_t w4_BRs_a = m_br_4/PDG_BRs_4_a;
	
	Double_t w0_BRs_b = m_br_0/PDG_BRs_0_b;
	Double_t w1_BRs_b = m_br_1/PDG_BRs_1_b;
	Double_t w2_BRs_b = m_br_2/PDG_BRs_2_b;
	Double_t w3_BRs_b = m_br_3/PDG_BRs_3_b;
	Double_t w4_BRs_b = m_br_4/PDG_BRs_4_b;
	
	Double_t w0_BRs_c = m_br_0/PDG_BRs_0_c;
	Double_t w1_BRs_c = m_br_1/PDG_BRs_1_c;
	Double_t w2_BRs_c = m_br_2/PDG_BRs_2_c;
	Double_t w3_BRs_c = m_br_3/PDG_BRs_3_c;
	Double_t w4_BRs_c = m_br_4/PDG_BRs_4_c;
	
	Double_t w0_BRs_d = m_br_0/PDG_BRs_0_d;
	Double_t w1_BRs_d = m_br_1/PDG_BRs_1_d;
	Double_t w2_BRs_d = m_br_2/PDG_BRs_2_d;
	Double_t w3_BRs_d = m_br_3/PDG_BRs_3_d;
	Double_t w4_BRs_d = m_br_4/PDG_BRs_4_d;
	
	Double_t w0_BRs_e = m_br_0/PDG_BRs_0_e;
	Double_t w1_BRs_e = m_br_1/PDG_BRs_1_e;
	Double_t w2_BRs_e = m_br_2/PDG_BRs_2_e;
	Double_t w3_BRs_e = m_br_3/PDG_BRs_3_e;
	Double_t w4_BRs_e = m_br_4/PDG_BRs_4_e;
	
	Double_t w0_BRs_f = m_br_0/PDG_BRs_0_f;
	Double_t w1_BRs_f = m_br_1/PDG_BRs_1_f;
	Double_t w2_BRs_f = m_br_2/PDG_BRs_2_f;
	Double_t w3_BRs_f = m_br_3/PDG_BRs_3_f;
	Double_t w4_BRs_f = m_br_4/PDG_BRs_4_f;
	
	Double_t w0_BRs_g = m_br_0/PDG_BRs_0_g;
	Double_t w1_BRs_g = m_br_1/PDG_BRs_1_g;
	Double_t w2_BRs_g = m_br_2/PDG_BRs_2_g;
	Double_t w3_BRs_g = m_br_3/PDG_BRs_3_g;
	Double_t w4_BRs_g = m_br_4/PDG_BRs_4_g;

	Double_t w0_BRs_h = m_br_0/PDG_BRs_0_h;
	Double_t w1_BRs_h = m_br_1/PDG_BRs_1_h;
	Double_t w2_BRs_h = m_br_2/PDG_BRs_2_h;
	Double_t w3_BRs_h = m_br_3/PDG_BRs_3_h;
	Double_t w4_BRs_h = m_br_4/PDG_BRs_4_h;
	
	Double_t w0_BRs_i = m_br_0/PDG_BRs_0_i;
	Double_t w1_BRs_i = m_br_1/PDG_BRs_1_i;
	Double_t w2_BRs_i = m_br_2/PDG_BRs_2_i;
	Double_t w3_BRs_i = m_br_3/PDG_BRs_3_i;
	Double_t w4_BRs_i = m_br_4/PDG_BRs_4_i;

	Double_t w0_BRs_l = m_br_0/PDG_BRs_0_l;
	Double_t w1_BRs_l = m_br_1/PDG_BRs_1_l;
	Double_t w2_BRs_l = m_br_2/PDG_BRs_2_l;
	Double_t w3_BRs_l = m_br_3/PDG_BRs_3_l;
	Double_t w4_BRs_l = m_br_4/PDG_BRs_4_l;
	
	m_histos["BRs_SF"]->Fill(1.,1./w0_BRs);
	m_histos["BRs_SF"]->Fill(2.,1./w1_BRs);
	m_histos["BRs_SF"]->Fill(3.,1./w2_BRs);
	m_histos["BRs_SF"]->Fill(4.,1./w3_BRs);
	m_histos["BRs_SF"]->Fill(5.,1./w4_BRs);

	m_histos["BRs_SF_bup"]->Fill(1.,1./w0_BRs_a);
	m_histos["BRs_SF_bup"]->Fill(2.,1./w1_BRs_a);
	m_histos["BRs_SF_bup"]->Fill(3.,1./w2_BRs_a);
	m_histos["BRs_SF_bup"]->Fill(4.,1./w3_BRs_a);
	m_histos["BRs_SF_bup"]->Fill(5.,1./w4_BRs_a);

	m_histos["BRs_SF_bdown"]->Fill(1.,1./w0_BRs_b);
	m_histos["BRs_SF_bdown"]->Fill(2.,1./w1_BRs_b);
	m_histos["BRs_SF_bdown"]->Fill(3.,1./w2_BRs_b);
	m_histos["BRs_SF_bdown"]->Fill(4.,1./w3_BRs_b);
	m_histos["BRs_SF_bdown"]->Fill(5.,1./w4_BRs_b);

	m_histos["BRs_SF_btauup"]->Fill(1.,1./w0_BRs_c);
	m_histos["BRs_SF_btauup"]->Fill(2.,1./w1_BRs_c);
	m_histos["BRs_SF_btauup"]->Fill(3.,1./w2_BRs_c);
	m_histos["BRs_SF_btauup"]->Fill(4.,1./w3_BRs_c);
	m_histos["BRs_SF_btauup"]->Fill(5.,1./w4_BRs_c);

	m_histos["BRs_SF_btaudown"]->Fill(1.,1./w0_BRs_d);
	m_histos["BRs_SF_btaudown"]->Fill(2.,1./w1_BRs_d);
	m_histos["BRs_SF_btaudown"]->Fill(3.,1./w2_BRs_d);
	m_histos["BRs_SF_btaudown"]->Fill(4.,1./w3_BRs_d);
	m_histos["BRs_SF_btaudown"]->Fill(5.,1./w4_BRs_d);

	m_histos["BRs_SF_btocup"]->Fill(1.,1./w0_BRs_e);
	m_histos["BRs_SF_btocup"]->Fill(2.,1./w1_BRs_e);
	m_histos["BRs_SF_btocup"]->Fill(3.,1./w2_BRs_e);
	m_histos["BRs_SF_btocup"]->Fill(4.,1./w3_BRs_e);
	m_histos["BRs_SF_btocup"]->Fill(5.,1./w4_BRs_e);

	m_histos["BRs_SF_btocdown"]->Fill(1.,1./w0_BRs_f);
	m_histos["BRs_SF_btocdown"]->Fill(2.,1./w1_BRs_f);
	m_histos["BRs_SF_btocdown"]->Fill(3.,1./w2_BRs_f);
	m_histos["BRs_SF_btocdown"]->Fill(4.,1./w3_BRs_f);
	m_histos["BRs_SF_btocdown"]->Fill(5.,1./w4_BRs_f);

	m_histos["BRs_SF_btoc2up"]->Fill(1.,1./w0_BRs_g);
	m_histos["BRs_SF_btoc2up"]->Fill(2.,1./w1_BRs_g);
	m_histos["BRs_SF_btoc2up"]->Fill(3.,1./w2_BRs_g);
	m_histos["BRs_SF_btoc2up"]->Fill(4.,1./w3_BRs_g);
	m_histos["BRs_SF_btoc2up"]->Fill(5.,1./w4_BRs_g);

	m_histos["BRs_SF_btoc2down"]->Fill(1.,1./w0_BRs_h);
	m_histos["BRs_SF_btoc2down"]->Fill(2.,1./w1_BRs_h);
	m_histos["BRs_SF_btoc2down"]->Fill(3.,1./w2_BRs_h);
	m_histos["BRs_SF_btoc2down"]->Fill(4.,1./w3_BRs_h);
	m_histos["BRs_SF_btoc2down"]->Fill(5.,1./w4_BRs_h);

	m_histos["BRs_SF_cup"]->Fill(1.,1./w0_BRs_i);
	m_histos["BRs_SF_cup"]->Fill(2.,1./w1_BRs_i);
	m_histos["BRs_SF_cup"]->Fill(3.,1./w2_BRs_i);
	m_histos["BRs_SF_cup"]->Fill(4.,1./w3_BRs_i);
	m_histos["BRs_SF_cup"]->Fill(5.,1./w4_BRs_i);

	m_histos["BRs_SF_cdown"]->Fill(1.,1./w0_BRs_l);
	m_histos["BRs_SF_cdown"]->Fill(2.,1./w1_BRs_l);
	m_histos["BRs_SF_cdown"]->Fill(3.,1./w2_BRs_l);
	m_histos["BRs_SF_cdown"]->Fill(4.,1./w3_BRs_l);
	m_histos["BRs_SF_cdown"]->Fill(5.,1./w4_BRs_l);

	//cout<<"end of Sample::calcHistos() "<<m_dsid<<endl;
	
}

void Sample::writeToFile(TString infilename)
{
	//cout<<"calling Sample::writeToFile() "<<m_dsid<<" "<<infilename<<endl;
	TFile *fout= TFile::Open(infilename,"UPDATE");
	for(map<TString,TH1D*>::iterator it=m_histos.begin(); it!=m_histos.end(); it++)
	{
		fout->WriteTObject(it->second);
	}
	delete fout;
	//cout<<"end of Sample::writeToFile()" <<endl;
	
}

void Sample::InitCPFHistos()
{
	//cout<<"calling Sample::InitCPFHistos()"<<m_dsid<<endl;
	m_histos["prodCfrac_SF"]= new TH1D("h_prodCfrac_SF_"+m_dsid,"h_prodCfrac_SF_"+m_dsid,4,0.5,4.5); 
	m_histos["prodCfrac_SF_Dplusup"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_Dplusup","h_prodCfrac_SF_"+m_dsid+"_Dplusup",4,0.5,4.5);	
	m_histos["prodCfrac_SF_Dplusdown"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_Dplusdown","h_prodCfrac_SF_"+m_dsid+"_Dplusdown",4,0.5,4.5);
	m_histos["prodCfrac_SF_D0up"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_D0up","h_prodCfrac_SF_"+m_dsid+"_D0up",4,0.5,4.5);
	m_histos["prodCfrac_SF_D0down"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_D0down","h_prodCfrac_SF_"+m_dsid+"_D0down",4,0.5,4.5);
	m_histos["prodCfrac_SF_Dsup"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_Dsup","h_prodCfrac_SF_"+m_dsid+"_Dsup",4,0.5,4.5);
	m_histos["prodCfrac_SF_Dsdown"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_Dsdown","h_prodCfrac_SF_"+m_dsid+"_Dsdown",4,0.5,4.5);
	m_histos["prodCfrac_SF_CBaryonup"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_CBaryonup","h_prodCfrac_SF_"+m_dsid+"_CBaryonup",4,0.5,4.5);
	m_histos["prodCfrac_SF_CBaryondown"]= new TH1D("h_prodCfrac_SF_"+m_dsid+"_CBaryondown","h_prodCfrac_SF_"+m_dsid+"_CBaryondown",4,0.5,4.5);
	
}

void Sample::InitBRHistos()
{
	//cout<<"calling Sample::InitBRHistos()"<<m_dsid<<endl;
	m_histos["BRs_SF"]= new TH1D("h_BRs_SF_"+m_dsid,"h_BRs_SF_"+m_dsid,5,0.5,5.5); 
	m_histos["BRs_SF_bup"]= new TH1D("h_BRs_SF_"+m_dsid+"_bup","h_BRs_SF_"+m_dsid+"_bup",5,0.5,5.5); 
	m_histos["BRs_SF_bdown"]= new TH1D("h_BRs_SF_"+m_dsid+"_bdown","h_BRs_SF_"+m_dsid+"_bdown",5,0.5,5.5); 
	m_histos["BRs_SF_btauup"]= new TH1D("h_BRs_SF_"+m_dsid+"_btauup","h_BRs_SF_"+m_dsid+"_btauup",5,0.5,5.5); 
	m_histos["BRs_SF_btaudown"]= new TH1D("h_BRs_SF_"+m_dsid+"_btaudown","h_BRs_SF_"+m_dsid+"_btaudown",5,0.5,5.5); 
	m_histos["BRs_SF_btocup"]= new TH1D("h_BRs_SF_"+m_dsid+"_btocup","h_BRs_SF_"+m_dsid+"_btocup",5,0.5,5.5); 
	m_histos["BRs_SF_btocdown"]= new TH1D("h_BRs_SF_"+m_dsid+"_btocdown","h_BRs_SF_"+m_dsid+"_btocdown",5,0.5,5.5); 
	m_histos["BRs_SF_btoc2up"]= new TH1D("h_BRs_SF_"+m_dsid+"_btoc2up","h_BRs_SF_"+m_dsid+"_btoc2up",5,0.5,5.5); 
	m_histos["BRs_SF_btoc2down"]= new TH1D("h_BRs_SF_"+m_dsid+"_btoc2down","h_BRs_SF_"+m_dsid+"_btoc2down",5,0.5,5.5);
	m_histos["BRs_SF_cup"]= new TH1D("h_BRs_SF_"+m_dsid+"_cup","h_BRs_SF_"+m_dsid+"_cup",5,0.5,5.5); 
	m_histos["BRs_SF_cdown"]= new TH1D("h_BRs_SF_"+m_dsid+"_cdown","h_BRs_SF_"+m_dsid+"_cdown",5,0.5,5.5);
}

void Sample::InitPFHistos()
{
	//cout<<"calling Sample::InitPFHistos()"<<m_dsid<<endl;
	m_histos["prodfrac_SF"]= new TH1D("h_prodfrac_SF_"+m_dsid,"h_prodfrac_SF_"+m_dsid,5,0.5,5.5); 
	m_histos["prodfrac_SF_Bup"]= new TH1D("h_prodfrac_SF_"+m_dsid+"_Bup","h_prodfrac_SF_"+m_dsid+"_Bup",5,0.5,5.5); 
	m_histos["prodfrac_SF_Bdown"]= new TH1D("h_prodfrac_SF_"+m_dsid+"_Bdown","h_prodfrac_SF_"+m_dsid+"_Bdown",5,0.5,5.5); 
	m_histos["prodfrac_SF_Bsup"]= new TH1D("h_prodfrac_SF_"+m_dsid+"_Bsup","h_prodfrac_SF_"+m_dsid+"_Bsup",5,0.5,5.5); 
	m_histos["prodfrac_SF_Bsdown"]= new TH1D("h_prodfrac_SF_"+m_dsid+"_Bsdown","h_prodfrac_"+m_dsid+"_SF_Bsdown",5,0.5,5.5); 
	m_histos["prodfrac_SF_Baryup"]= new TH1D("h_prodfrac_SF_"+m_dsid+"_Baryup","h_prodfrac_SF_"+m_dsid+"_Baryup",5,0.5,5.5); 
	m_histos["prodfrac_SF_Barydown"]= new TH1D("h_prodfrac_SF_"+m_dsid+"_Barydown","h_prodfrac_SF_"+m_dsid+"_Barydown",5,0.5,5.5); 
	
}

#endif
