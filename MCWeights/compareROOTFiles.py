from ROOT import *

file1 = TFile.Open("MCweights_v241018v4.root","READ")
file2 = TFile.Open("MCweights_v241018v5Test.root","READ")

#file1.cd()
count1=0
count2=0
for h in file1.GetListOfKeys():
	count1+=1

for h in file2.GetListOfKeys():
	count2+=1

print file1.GetName()," has nobjects=",count1
print file2.GetName()," has nobjects=",count2

for h in file1.GetListOfKeys():
    h = h.ReadObj()
    h2 = file2.Get(h.GetName())
    #print h.ClassName(), h.GetName(), h2.ClassName(), h2.GetName()
    if (h.ClassName()=="TH1D" or h.ClassName()=="TH1F" ):
		#print "I'm here"
		for ibin in range(0,h.GetNbinsX()+1):
			#print h.GetName()," bin=",ibin," ",h.GetBinContent(ibin)," ",h2.GetBinContent(ibin)
			if(h.GetBinContent(ibin)!=h2.GetBinContent(ibin)):
				print "ERROR for bin ",i," of histo ",h.GetName()
