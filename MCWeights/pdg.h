#ifndef __PDG__
#define __PDG__

#include <iostream>

using namespace std;

	////////////////////////////////////////////
	///////////// PF ///////////////////////////
	////////////////////////////////////////////
	
	const double PDG_0 = 0.404; // B0
	const double PDG_1 = 0.404; // B+
	const double PDG_2 = 0.103; // Bs
	const double PDG_4 = 0.088; // BARYONS

	const double PDG_0_a = 0.410; // B0
	const double PDG_1_a = 0.410; // B+
	const double PDG_2_a = 0.102; // Bs
	const double PDG_4_a = 0.077; // BARYONS

	// SET 2 B0/B+ DOWN

	const double PDG_0_b = 0.398; // B0
	const double PDG_1_b = 0.398; // B+
	const double PDG_2_b = 0.108; // Bs
	const double PDG_4_b = 0.099; // BARYONS

	// SET 3 Bs UP

	const double PDG_0_c = 0.403; // B0
	const double PDG_1_c = 0.403; // B+
	const double PDG_2_c = 0.108; // Bs
	const double PDG_4_c = 0.085; // BARYONS

	// SET 4 Bs DOWN

	const double PDG_0_d = 0.405; // B0
	const double PDG_1_d = 0.405; // B+
	const double PDG_2_d = 0.098; // Bs
	const double PDG_4_d = 0.091; // BARYONS

	// SET 5 BARY UP

	const double PDG_0_e = 0.398; // B0
	const double PDG_1_e = 0.398; // B+
	const double PDG_2_e = 0.102; // Bs
	const double PDG_4_e = 0.100; // BARYONS

	// SET 6 BARY DOWN

	const double PDG_0_f = 0.410; // B0
	const double PDG_1_f = 0.410; // B+
	const double PDG_2_f = 0.104; // Bs
	const double PDG_4_f = 0.076; // BARYONS
	
	
	////////////////////////////////////////////
	///////////// BR ///////////////////////////
	////////////////////////////////////////////
	
	
	const double PDG_BRs_0 = 0.1095; // b to mu
	const double PDG_BRs_1 = 0.0042; // b to tau to mu
	const double PDG_BRs_2 = 0.0802; // b to c to mu
	const double PDG_BRs_3 = 0.016155; // b to ccbar to mu
	const double PDG_BRs_4 = 0.082; // c to mu

	// VARIATIONS

	// SET 1  UP

	const double PDG_BRs_0_a = 0.1124; // b to mu
	const double PDG_BRs_1_a = 0.0042; // b to tau to mu
	const double PDG_BRs_2_a = 0.0802; // b to c to mu
	const double PDG_BRs_3_a = 0.01615; // b to ccbar to mu
	const double PDG_BRs_4_a = 0.082; // c to mu

	// SET 1 DOWN

	const double PDG_BRs_0_b = 0.1070; // b to mu
	const double PDG_BRs_1_b = 0.0042; // b to tau to mu
	const double PDG_BRs_2_b = 0.0802; // b to c to mu
	const double PDG_BRs_3_b = 0.01615; // b to ccbar to mu
	const double PDG_BRs_4_b = 0.082; // c to mu

	// SET 2  UP

	const double PDG_BRs_0_c = 0.1095; // b to mu
	const double PDG_BRs_1_c = 0.0046; // b to tau to mu
	const double PDG_BRs_2_c = 0.0802; // b to c to mu
	const double PDG_BRs_3_c = 0.01615; // b to ccbar to mu
	const double PDG_BRs_4_c = 0.082; // c to mu

	// SET 2 DOWN

	const double PDG_BRs_0_d = 0.1095; // b to mu
	const double PDG_BRs_1_d = 0.0038; // b to tau to mu
	const double PDG_BRs_2_d = 0.0802; // b to c to mu
	const double PDG_BRs_3_d = 0.01615; // b to cbar to mu
	const double PDG_BRs_4_d = 0.082; // c to mu

	// SET 3  UP

	const double PDG_BRs_0_e = 0.1095; // b to mu
	const double PDG_BRs_1_e = 0.0042; // b to tau to mu
	const double PDG_BRs_2_e = 0.0821; // b to c to mu
	const double PDG_BRs_3_e = 0.01615; // b to cbar to mu
	const double PDG_BRs_4_e = 0.082; // c to mu

	// SET 3 DOWN

	const double PDG_BRs_0_f = 0.1095; // b to mu
	const double PDG_BRs_1_f = 0.0042; // b to tau to mu
	const double PDG_BRs_2_f = 0.0783; // b to c to mu
	const double PDG_BRs_3_f = 0.01615; // b to cbar to mu
	const double PDG_BRs_4_f = 0.082; // c to mu


	// SET 4  UP

	const double PDG_BRs_0_g = 0.1095; // b to mu
	const double PDG_BRs_1_g = 0.0042; // b to tau to mu
	const double PDG_BRs_2_g = 0.0802; // b to c to mu
	const double PDG_BRs_3_g = 0.01915; // b to cbar to mu
	const double PDG_BRs_4_g = 0.082; // c to mu

	// SET 4 DOWN

	const double PDG_BRs_0_h = 0.1095; // b to mu
	const double PDG_BRs_1_h = 0.0042; // b to tau to mu
	const double PDG_BRs_2_h = 0.0802; // b to c to mu
	const double PDG_BRs_3_h = 0.01315; // b to cbar to mu
	const double PDG_BRs_4_h = 0.082; // c to mu

	// SET 5  UP

	const double PDG_BRs_0_i = 0.1095; // b to mu
	const double PDG_BRs_1_i = 0.0042; // b to tau to mu
	const double PDG_BRs_2_i = 0.0802; // b to c to mu
	const double PDG_BRs_3_i = 0.01615; // b to cbar to mu
	const double PDG_BRs_4_i = 0.087; // c to mu

	// SET 5 DOWN

	const double PDG_BRs_0_l = 0.1095; // b to mu
	const double PDG_BRs_1_l = 0.0042; // b to tau to mu
	const double PDG_BRs_2_l = 0.0802; // b to c to mu
	const double PDG_BRs_3_l = 0.01615; // b to cbar to mu
	const double PDG_BRs_4_l = 0.077; // c to mu
	
	////////////////////////////////////////////
	///////////// CPF //////////////////////////
	////////////////////////////////////////////
	
	const double CPDG_411 = 0.2256; // D+
	const double CPDG_421 = 0.564; // D0
	const double CPDG_431 = 0.0797; // Ds
	const double CPDG_4122 = 0.1089; // C-BARYONS
	
	//VARIATIONS
	
	//D+ up
	const double CPDG_411_a = 0.2256+sqrt(pow(0.0077,2)+pow(0.010,2)); // D+
	const double CPDG_421_a = 0.564; // D0
	const double CPDG_431_a = 0.0797; // Ds
	const double CPDG_4122_a = 0.1089; // C-BARYONS
	
	//D+ down
	const double CPDG_411_b = 0.2256-sqrt(pow(0.0077,2)+pow(0.010,2)); // D+
	const double CPDG_421_b = 0.564; // D0
	const double CPDG_431_b = 0.0797; // Ds
	const double CPDG_4122_b = 0.1089; // C-BARYONS
	
	//D0 up
	const double CPDG_411_c = 0.2256; // D+
	const double CPDG_421_c = 0.564+sqrt(pow(0.0151,2)+pow(0.0135,2)); // D0
	const double CPDG_431_c = 0.0797; // Ds
	const double CPDG_4122_c = 0.1089; // C-BARYONS
	
	//D0 down
	const double CPDG_411_d = 0.2256; // D+
	const double CPDG_421_d = 0.564-sqrt(pow(0.0151,2)+pow(0.0164,2)); // D0
	const double CPDG_431_d = 0.0797; // Ds
	const double CPDG_4122_d = 0.1089; // C-BARYONS
	
	//Ds up
	const double CPDG_411_e = 0.2256; // D+
	const double CPDG_421_e = 0.564; // D0
	const double CPDG_431_e = 0.0797+sqrt(pow(0.0045,2)+pow(0.0052,2)); // Ds
	const double CPDG_4122_e = 0.1089; // C-BARYONS
	
	//Ds down
	const double CPDG_411_f = 0.2256; // D+
	const double CPDG_421_f = 0.564; // D0
	const double CPDG_431_f = 0.0797-sqrt(pow(0.0045,2)+pow(0.0046,2)); // Ds
	const double CPDG_4122_f = 0.1089; // C-BARYONS
	
	//C_baryons up
	const double CPDG_411_g = 0.2256; // D+
	const double CPDG_421_g = 0.564; // D0
	const double CPDG_431_g = 0.0797; // Ds
	const double CPDG_4122_g = 0.1089+sqrt(pow(0.0091,2)+pow(0.0279,2)); // C-BARYONS
	
	//C_baryons down
	const double CPDG_411_h = 0.2256; // D+
	const double CPDG_421_h = 0.564; // D0
	const double CPDG_431_h = 0.0797; // Ds
	const double CPDG_4122_h = 0.1089-sqrt(pow(0.0091,2)+pow(0.0174,2)); // C-BARYONS

#endif
