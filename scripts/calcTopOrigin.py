#####
from ROOT import *

REGION="OS"
FOLDER="../../nptev_V21_slim_fit_SameTopOrigin/Histograms/"

fAll=TFile.Open(FOLDER+"/nptev_V21_slim_fit_SameTopOrigin_All"+REGION+"_histos.root","READ")
fSameTop=TFile.Open(FOLDER+"/nptev_V21_slim_fit_SameTopOrigin_SameTop"+REGION+"_histos.root","READ")
fDifferentTop=TFile.Open(FOLDER+"/nptev_V21_slim_fit_SameTopOrigin_DifferentTop"+REGION+"_histos.root","READ")
fNoTop=TFile.Open(FOLDER+"/nptev_V21_slim_fit_SameTopOrigin_NoTop"+REGION+"_histos.root","READ")

All=fAll.Get("All"+REGION+"_ttbar").Integral()
SameTop=fSameTop.Get("SameTop"+REGION+"_ttbar").Integral()
DifferentTop=fDifferentTop.Get("DifferentTop"+REGION+"_ttbar").Integral()
NoTop=fNoTop.Get("NoTop"+REGION+"_ttbar").Integral()

print("SameTop: %.2f %%" % (100.*SameTop/All))
print("DifferentTop: %.2f %%" % (100.*DifferentTop/All))
print("NoTop: %.2f %%" % (100.*NoTop/All))

print "check: ",((SameTop+DifferentTop+NoTop)/All)
