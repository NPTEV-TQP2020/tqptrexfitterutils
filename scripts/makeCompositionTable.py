#####
from ROOT import *

REGION="SS"

FILENAME="../../nptev_V21_slim_fit_Composition"+REGION+"/Histograms/nptev_V21_slim_fit_Composition"+REGION+"_Composition_histos.root"
f= TFile.Open(FILENAME,"READ")
h= f.Get("Composition_ttbar")

integral = h.Integral()
#print "fakes ",(h.GetBinContent(h.GetXaxis().FindBin(0))/integral) 
sssum=100.*(h.GetBinContent(h.GetXaxis().FindBin(1))+h.GetBinContent(h.GetXaxis().FindBin(10))+h.GetBinContent(h.GetXaxis().FindBin(100))+h.GetBinContent(h.GetXaxis().FindBin(110))+h.GetBinContent(h.GetXaxis().FindBin(1001))+h.GetBinContent(h.GetXaxis().FindBin(1002))+h.GetBinContent(h.GetXaxis().FindBin(1003))+h.GetBinContent(h.GetXaxis().FindBin(1004))+h.GetBinContent(h.GetXaxis().FindBin(10041))+h.GetBinContent(h.GetXaxis().FindBin(10042))+h.GetBinContent(h.GetXaxis().FindBin(10043)) )/integral

print "\\begin{table}[htbp]"
print "\\begin{center}"
print "\\begin{tabular}{|c|c|c|}"
print "\\hline"
print("\\multirow{4}{*}{\\ttbar{} SMT-signal} & $t \\rightarrow b$, $b\\rightarrow B$, $B\\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*h.GetBinContent(h.GetXaxis().FindBin(1))/integral)) #73.67
print ""
print("& $t \\rightarrow b$, $b\\rightarrow B$, $B\\rightarrow C$, $C\\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*h.GetBinContent(h.GetXaxis().FindBin(10))/integral)) #16.52
print ""
print(" & $t \\rightarrow b$, $b\\rightarrow B$, $B\\rightarrow \\tau$, $\\tau \\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*h.GetBinContent(h.GetXaxis().FindBin(100))/integral)) #1.95
print ""
print("& $t \\rightarrow b$, $b\\rightarrow B$, $B\\rightarrow C$, $C\\rightarrow \\tau$, $\\tau \\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*h.GetBinContent(h.GetXaxis().FindBin(110))/integral)) #0.77
print "\\hline"
print("\\multirow{4}{*}{\\ttbar{} SMT-bkg} & $B$ not coming from $t \\rightarrow b$ decay chain, $B\\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*h.GetBinContent(h.GetXaxis().FindBin(1001))/integral)) #0.62
print ""
print(" & $C$ not coming from $t \\rightarrow b$ decay chain, $C\\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*(h.GetBinContent(h.GetXaxis().FindBin(1002))+h.GetBinContent(h.GetXaxis().FindBin(1003)))/integral)) #5.81
print ""
print(" & $\\tau$ not coming from $t \\rightarrow b$ decay chain, $\\tau \\rightarrow \\mu$ & %.2f \\%% \\\\" % (100.*(h.GetBinContent(h.GetXaxis().FindBin(1004))+h.GetBinContent(h.GetXaxis().FindBin(10041))+h.GetBinContent(h.GetXaxis().FindBin(10042))+h.GetBinContent(h.GetXaxis().FindBin(10043)))/integral)) #0.66
print ""
print "\\hline"
print(" & Sum  & %.2f \\%%  \\\\" % sssum)
print "\\hline"
print "\\end{tabular}"
print("\\caption{Fraction of MC \\ttbar\\ events after final selection cuts are applied in the "+REGION+" region.} ")
print("\\label{tab:fraction_optim"+REGION+"}")
print "\\end{center}"
print "\\end{table}"
