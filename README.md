#summary of config files
in folder helperConfigFiles/ you have config files for yield tables, composition tables and so on
in folder olderConfigFiles/ you have config files from previous versions
nptev_V21_slim_fit.config is the default config file
nptev_V21_slim_smtcalib_fit.config is to calculate smt calibration

# TQPTRExFitterUtils

**Setup**

  - Checkout the latest `TRExFitter` code:
  ```
  git clone ssh://git@gitlab.cern.ch:7999/TRExStats/TRExFitter.git
  ```

  - Checkout this package insde the main `TRExFitter` directory:
  ```
  cd TRExFitter/
  git clone ssh://git@gitlab.cern.ch:7999/NPTEV-TQP2020/tqptrexfitterutils.git
  ```
  
  - Follow the usual `TRExFitter` setup procedure:
  ```
  git submodule init
  git submodule update
  source setup.sh
  mkdir build && cd build
  cmake ../
  cmake --build ./
  ```
  
  - To just update the two codes from git, use git clone in the main `TRExFitter` directory and then inside the `tqptrexfitterutils` directory
  


**Basics**

  - To create the histograms:
    ```
    trex-fitter n config/nptev_V18_fit.config
    ```
    (better to do it as a batch job, using the script)
  * to do a test without systematics (recommended):
    ```
    trex-fitter n config/nptev_V18_fit.config systematics=none
    ```

  - To do the full fit:
    ```
    trex-fitter wf config/nptev_V18_fit.config
    ```
  * to exclude some systematics:
    ```
    trex-fitter wf config/nptev_V18_fit.config Exclude=syst1,syst2,...
    ```
  * to run with some systematics only:
    ```
    trex-fitter wf config/nptev_V18_fit.config Systematics=syst1,syst2,...
    ```

  - Notes:
  * the default mode should be `DoNonProfileFit: TRUE`, with `FitBlind: TRUE`
  * ...
  
    
**For plots**

  - Look at the config file "..."

  - One can do a plot using an alternative signal sample (i.e. one of the ttbar samples set as GHOST in the config file) by adding as command-line option:
  `Signals=ttbar1,ttbar2,...`

  - To scale the sum of all the ttbar samples to data minus background, use/uncomment the line 
  ScaleSamplesToData: "ttbar","ttbar_fake","ttbar_dilep","ttbar_ghost_btomu",...
  making sure that the sameple(s) you use as signal are listed here (you will see something printed n the screen indicating how much each sample gets scaled)

  - For things like jet pT scaling, what I did was to use the functionality `VariableForSample: <sample>:<variableExpression>`
  e.g.
    ```
    Region: "jet_pt_optiSel"
      Variable: "jet_pt[0]/1e3",30,0,300
      VariableForSample: "ttbar_ghost_scaled":"(jet_pt[0]*0.95)/1e3"
    ```
  plus something properly set in order to apply also the correct selection to the scaled sample
  (at the moment done with SelectionForSample under Region, but probably better to set it under the sample, like:
    ```
    Sample: "ttbar_ghost_scaled"
      Selection: "nSMT>=1 && nBTags_77>=1 && met_met>30e3 && (met_met+mtw)>60e3 && SMTmu_pt>8e3 && (jet_pt[3]*0.95>30e3 || (SMTmu_jetIdx==3 && jet_pt[2]*0.95>30e3)) && SMTmu_charge*lep_charge<0 &&  DeltaR(lep_pt,lep_eta,lep_phi,SMTmu_pt,SMTmu_eta,SMTmu_phi)<2."
    ```
  even if in this case one has to make sure is making all the plots with the same selection... Hacky in any case...)

  
**Few other explanations**

  - `KeepNormForSamples` is basically making a certain systematic shape-only for the sum of samples specified (in our case the 3 ttbar samples, that are all scaled by the norm factor k_tt)

  - `DummyForSamples` is a new option I had to add in order to correctly make systematics that are aff....

## NTupleSlimmer
This is a small python script which allows you to easily slim and skim an ntuple. It is based on ROOT's `RDataFrame`. Because of this there are occasional crashes which don't seem to make sense. I have tested this in all use cases I can think of and it works 95% of the time. If it does at any point crash, make sure to delete the output file and try again, it will generally work on second attempt.

I have been testing in `root-6.16.00` but it also seems to work fine in `root-6.14.04`. To setup, all that is needed is:
```
setupATLAS
lsetup "root 6.16.00-x86_64-slc6-gcc62-opt"
```
The script itself takes 4 arguments:

| **Option** | **Usage** |
| ---------- | ---------- |
| `-t` | Name of the tree you want to loop over. The output tree will also have this name. |
| `-f` | The path and name of input file. Wildcards can also be used but make sure to put quote marks around path if doing so. |
| `-o` | Path and name of output file. |
| `-c` | Path and name of config file without `.py` extension. Config file must be in the same directory as the script. |

The config file is just a python dictionary in a separate file, `ntuple_slimmer_config.py` is an example. The config has the following options:

| **Option** | **Usage** |
| ---------- | ---------- |
| `branches` | A list of the branches you wish to keep in both reco and particle trees. Can use wildcards, e.g. for btag systeamtics `weight_btagSF*`. |
| `reco_branches` | A list of branches you wish to keep which are only in reco trees. |
| `particle_branches` | A list of branches you wish to keep which are only in particle trees. |
| `reco_cuts` | String with the reco cuts in. |
| `particle_cuts` | String with the particle cuts in. |
| `reco_weight` | String with reco weights in. The weights will be combined into a single variable named `weight`. |
| `particle_weight` | String with particle weights in. The weights will be combined into a single variable named `weight`. |
| `reco_new_vars` | New reco variables to be created. This is a list of 2 entry lists, one entry for the name of the new variable and one for the expression to create it. |
| `particle_new_vars` | New particle variables to be created. This is a list of 2 entry lists, one entry for the name of the new variable and one for the expression to create it. |
| `functions` | List of the ROOT macros for the cuts and weights. |

Line 48, `r.ROOT.EnableImplicitMT()` is currently commented out. In principle you should be able to take advantage of MT but I have found issues with this. You are welcome to try it but even without it, even the large files are taking under 5 mins to run.

I have not attached a script to run over multiple files because I am not sure on the batch system you have at Rome. It will be very straightforward to write one, the only thing to remember is to change the `-t` argument depending on if you are running on reco or particle. Currently the code assumes reco trees are `nominal` and particle level is `particleLevelTree`.
